﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class InstellingenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public InstellingenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Instellingen
        public async Task<IActionResult> Index()
        {
              return View(await _context.Instellingen.ToListAsync());
        }

        // GET: Instellingen/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Instellingen == null)
            {
                return NotFound();
            }

            var tblInstellingen = await _context.Instellingen
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblInstellingen == null)
            {
                return NotFound();
            }

            return View(tblInstellingen);
        }

        // GET: Instellingen/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Instellingen/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naam,Straat,Huisnr,Postcode,Stad,Email,Telefoon,TijdslotInterval")] TblInstellingen tblInstellingen)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblInstellingen);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblInstellingen);
        }

        // GET: Instellingen/Edit/5
        public async Task<IActionResult> Edit()
        {
            //A>Get data uit database
            //--------------------------
            int id = _context.Instellingen.FirstOrDefault().Id;
            var tblInstellingen = await _context.Instellingen.FindAsync(id);

            if (tblInstellingen == null)
            {
                return NotFound();
            }

            //B>Vul model
            //--------------
            InstellingenModel model = new()
            {
                Id = id,        
                Naam = tblInstellingen.Naam,
                Straat = tblInstellingen.Straat,
                Huisnr = tblInstellingen.Huisnr,
                Postcode = tblInstellingen.Postcode,
                Stad = tblInstellingen.Stad,

                Email = tblInstellingen.Email,
                Telefoon = tblInstellingen.Telefoon,

                TijdslotInterval = tblInstellingen.TijdslotInterval,

                GetrouwheidScoreLimiet = tblInstellingen.GetrouwheidScoreLimiet,

                KortingWaardeInDecimaal = (tblInstellingen.KortingWaardeInDecimaal).ToString(),

                BtwInDecimaal = ( tblInstellingen.BtwInDecimaal).ToString(),

                AfzenderAdres = tblInstellingen.AfzenderAdres,
                AfzenderNaam = tblInstellingen.AfzenderNaam,
                Onderwerp = tblInstellingen.Onderwerp,
                SmtpPort  = tblInstellingen.SmtpPort,
                SmtpHost = tblInstellingen.SmtpHost,
                Login = tblInstellingen.Login,
                Paswoord =  tblInstellingen.Paswoord

            };


            //> Layout data
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;

            return View(model);
        }

        // POST: Instellingen/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, InstellingenModel model)
        {
            
            //A> Data input
            //-------------
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //B> Vullen van het tblModel
                    //---------------------------

                    TblInstellingen tblInstellingen = new TblInstellingen()
                    {
                        Id = (int)id,
                        Naam = model.Naam,
                        Straat = model.Straat,
                        Huisnr = model.Huisnr,
                        Postcode = model.Postcode,
                        Stad = model.Stad,

                        Email = model.Email,
                        Telefoon = model.Telefoon,

                        TijdslotInterval = model.TijdslotInterval,

                        GetrouwheidScoreLimiet = model.GetrouwheidScoreLimiet,

                            
                        KortingWaardeInDecimaal = (float)Math.Round(float.Parse(model.KortingWaardeInDecimaal),2),//dit is een string dat naar float moet worden omgezet

                        BtwInDecimaal = (float)Math.Round(float.Parse(model.BtwInDecimaal),2)//dit is een string dat naar float moet worden omgezet
                    };


                    _context.Update(tblInstellingen);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblInstellingenExists(model.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(insertOkMsg));
            }
            return View(model);
        }

        public IActionResult insertOkMsg()
        {
            //Layout en rol data
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;

            return View();
        }

        // GET: Instellingen/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Instellingen == null)
            {
                return NotFound();
            }

            var tblInstellingen = await _context.Instellingen
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblInstellingen == null)
            {
                return NotFound();
            }

            return View(tblInstellingen);
        }

        // POST: Instellingen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Instellingen == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Instellingen'  is null.");
            }
            var tblInstellingen = await _context.Instellingen.FindAsync(id);
            if (tblInstellingen != null)
            {
                _context.Instellingen.Remove(tblInstellingen);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblInstellingenExists(int id)
        {
          return _context.Instellingen.Any(e => e.Id == id);
        }
    }
}
