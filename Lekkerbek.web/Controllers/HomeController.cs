﻿using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly LekkerbekDbContext _context;

        public HomeController(ILogger<HomeController> logger, LekkerbekDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index() { 

            ViewBag.rol = _context.Rol.ToList();   //get data personeel
            return View(); 
        }

        public IActionResult Home(String Id)
        {
            var loginData = _context.LoginData.FirstOrDefault();
            switch (Int32.Parse(Id))
            {
                case 1:
                    if (loginData != null)
                    {
                        loginData.Rol = "Administratief";
                        loginData.Layout = "~/Views/Shared/_LayoutAdministratief.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Administratief";
                        loginData.Layout = "~/Views/Shared/_LayoutAdministratief.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }

                    return View("Administratief");

                case 2:
                    if (loginData != null)
                    {
                        loginData.Rol = "Kassa";
                        loginData.Layout = "~/Views/Shared/_LayoutKassaMedewerker.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Kassa";
                        loginData.Layout = "~/Views/Shared/_LayoutKassaMedewerker.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }
                    return View("Kassa");

                case 3:
                    if (loginData != null)
                    {
                        loginData.Rol = "Kok";
                        loginData.Layout = "~/Views/Shared/_LayoutKok.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Kok";
                        loginData.Layout = "~/Views/Shared/_LayoutKok.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }
                    return View("Kok");

                case 4:
                    if (loginData != null)
                    {
                        loginData.Rol = "Admin";
                        loginData.Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Admin";
                        loginData.Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }
                    return View("Admin");

                default: return View();
                    // case "Kok": return View();
            }
        }


        [HttpPost]
        public IActionResult Index(String Id)
        {
            var loginData = _context.LoginData.FirstOrDefault();
            switch (Int32.Parse(Id))
            {
                case 1:
                    if(loginData != null)
                    {
                        loginData.Rol = "Administratief";
                        loginData.Layout = "~/Views/Shared/_LayoutAdministratief.cshtml";
                        
                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        TblLoginData data = new()
                        {
                            Rol = "Administratief",
                            Layout = "~/Views/Shared/_LayoutAdministratief.cshtml"
                        };
                        _context.Add(data);
                        _context.SaveChanges();
                    }
                        
                    return View("Administratief");
                        
                case 2:
                    if (loginData != null)
                    {
                        loginData.Rol = "Kassa";
                        loginData.Layout = "~/Views/Shared/_LayoutKassaMedewerker.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        TblLoginData data = new()
                        {
                            Rol = "Kassa",
                            Layout = "~/Views/Shared/_LayoutKassaMedewerker.cshtml"
                        };
                        _context.Add(data);
                        _context.SaveChanges();                      
                    }
                    return View("Kassa");

                case 3:
                    if (loginData != null)
                    {
                        loginData.Rol = "Kok";
                        loginData.Layout = "~/Views/Shared/_LayoutKok.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Kok";
                        loginData.Layout = "~/Views/Shared/_LayoutKok.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }
                    return View("Kok");

                case 4:
                    if (loginData != null)
                    {
                        loginData.Rol = "Admin";
                        loginData.Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

                        _context.Update(loginData);
                        _context.SaveChanges();
                    }
                    else
                    {
                        loginData.Rol = "Admin";
                        loginData.Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

                        _context.Add(loginData);
                        _context.SaveChanges();
                    }
                    return View("Admin");

                default:return View();
                    // case "Kok": return View();
            }
        }

        

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }




    }
}
