﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class BesteldeGerechtenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public BesteldeGerechtenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: BesteldeGerechten
        public IActionResult Index(int? id)
        {
            var idsBg = (from s in _context.BesteldeGerechten
                       join c in _context.Gerechten
                        on s.GerechtenId equals c.Id
                       where s.BestellingId == id
                       select s.Id).ToList();

            var gerechten = (from s in _context.BesteldeGerechten
                             join c in _context.Gerechten
                              on s.GerechtenId equals c.Id
                             where s.BestellingId == id
                             select new { c.GerechtNaam, c.Prijs }).ToList();

            var gerechtOpties = (from s in _context.BesteldeGerechten
                                 join c in _context.GerechtOpties
                                  on s.GerechtOptiesId equals c.Id
                                 where s.BestellingId == id
                                 select new { c.Optie, c.Prijs }).ToList();
            
            List<BesteldeGerechtenModel> listModels = new();

            for (int i = 0; i < gerechten.Count; i++)
            {
                BesteldeGerechtenModel model = new()
                {
                    Id = idsBg[i],
                    Gerecht = gerechten[i].GerechtNaam,
                    Optie = gerechtOpties[i].Optie,
                    BestellingId = (int)id,
                    
                };
                listModels.Add(model);
            }

            //Layout
            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;

            return View(listModels);
        }

        // GET: BesteldeGerechten/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblBesteldeGerechten = await _context.BesteldeGerechten
                .Include(t => t.Gerechten)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblBesteldeGerechten == null)
            {
                return NotFound();
            }

            return View(tblBesteldeGerechten);
        }

        // GET: BesteldeGerechten/Create

        public IActionResult Create()
        {

            var gerechten = new SelectList((from s in _context.Gerechten.ToList()
                                            select new { s.Id, s.GerechtNaam}), "Id", "GerechtNaam", null);
            //gerechten connectie
            BesteldeGerechtenModel model = new()
            {
                Gerechten = gerechten,
                Layout = _context.LoginData.FirstOrDefault().Layout
            };

            //gerechtOpties connectie
            var gerechtOpties = new SelectList((from c in _context.GerechtOpties.ToList()
                                                select new { c.Id, c.Optie }), "Id", "Optie", null);
            model.GerechtOpties = gerechtOpties;

            //bestellings connectie
            model.BestellingId = _context.Bestellingen.ToList().OrderByDescending(s => s.Id).FirstOrDefault().Id;


            return View(model);
        }



        // POST: BesteldeGerechten/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BesteldeGerechtenModel model)
        {
            TblBesteldeGerechten tblBesteldeGerechten = new()
            {
                GerechtenId = Int32.Parse(model.SelectedGerechtId),
                GerechtOptiesId = Int32.Parse(model.SelectedGerechtOptieId),
                BestellingId = model.BestellingId
            };

            if (ModelState.IsValid)
            {
                _context.Add(tblBesteldeGerechten);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = tblBesteldeGerechten.BestellingId });
            }
            ViewData["GerechtenId"] = new SelectList(_context.Gerechten, "Id", "Id", tblBesteldeGerechten.GerechtenId);
            return View(tblBesteldeGerechten);
        }

        // GET: BesteldeGerechten/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            else 
            {   //data in tblBesteldeGerechten
                var tblBesteldeGerechten = await _context.BesteldeGerechten.FindAsync(id);
                
                //Opslag container
                BesteldeGerechtenModel model = new();

                //gerechten connectie
                var gerechten = new SelectList((from s in _context.Gerechten.ToList()
                                               select new { s.Id, s.GerechtNaam }), "Id", "GerechtNaam", tblBesteldeGerechten.GerechtenId);
                model.Gerechten = gerechten;

                //gerechtOpties connectie
                var gerechtOpties = new SelectList((from c in _context.GerechtOpties.ToList()
                                                    select new { c.Id, c.Optie }), "Id", "Optie", tblBesteldeGerechten.GerechtOptiesId);
                model.GerechtOpties = gerechtOpties;

                //bestellings connectie
                model.BestellingId = tblBesteldeGerechten.BestellingId ;
               
               
                
                //ViewData["GerechtenId"] = new SelectList(_context.Gerechten, "Id", "Id", tblBesteldeGerechten.GerechtenId);
                return View(model);

            }

           
        }

        // POST: BesteldeGerechten/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BesteldeGerechtenModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }
            else 
            {
                var tblBesteldeGerechten = await _context.BesteldeGerechten.FindAsync(id);
                _context.BesteldeGerechten.Remove(tblBesteldeGerechten);
                await _context.SaveChangesAsync();

                TblBesteldeGerechten BesteldeGerechten = new()
                {
                    GerechtenId = Int32.Parse(model.SelectedGerechtId),
                    GerechtOptiesId = Int32.Parse(model.SelectedGerechtOptieId),
                    BestellingId = model.BestellingId
                };

                if (ModelState.IsValid)
                {
                    _context.Update(BesteldeGerechten);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index), new { id = tblBesteldeGerechten.BestellingId });
                }
            //ViewData["GerechtenId"] = new SelectList(_context.Gerechten, "Id", "Id", tblBesteldeGerechten.GerechtenId);
            return View(tblBesteldeGerechten);
            }
        }

        // GET: BesteldeGerechten/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblBesteldeGerechten = await _context.BesteldeGerechten
                .Include(t => t.Gerechten)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblBesteldeGerechten == null)
            {
                return NotFound();
            }

            return View(tblBesteldeGerechten);
        }

        // POST: BesteldeGerechten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblBesteldeGerechten = await _context.BesteldeGerechten.FindAsync(id);
            _context.BesteldeGerechten.Remove(tblBesteldeGerechten);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblBesteldeGerechtenExists(int id)
        {
            return _context.BesteldeGerechten.Any(e => e.Id == id);
        }
    }
}
