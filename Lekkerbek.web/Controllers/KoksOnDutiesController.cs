﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class KoksOnDutiesController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public KoksOnDutiesController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: KoksOnDuties
        public IActionResult Index()
        {
            var koks = _context.KoksOnDuties.ToList();
            List<KokInplanningModel> models = new();
            foreach(var item in koks)
            {
                KokInplanningModel model = new();
                model.Id = item.Id; 
                var naamA = _context.Personeel.Where(s => s.Id == item.KokA).FirstOrDefault();
                var naamB = _context.Personeel.Where(s => s.Id == item.KokB).FirstOrDefault();
                model.KokANaam = naamA.Voornaam + " " + naamA.Naam;
                                 
                model.KokBNaam = naamB.Voornaam + " " + naamB.Naam;

                model.Datum = item.Datum;   
                model.ShiftStart = item.ShiftStart; 
                model.ShiftStop = item.ShiftStop;
                models.Add(model);
            }
           
            

            return View(models);
        }

        // GET: KoksOnDuties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblKoksOnDuty = await _context.KoksOnDuties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblKoksOnDuty == null)
            {
                return NotFound();
            }

            return View(tblKoksOnDuty);
        }

        // GET: KoksOnDuties/Create
        public IActionResult Create()
        {
            ViewData["KokA"] = new SelectList((from s in _context.Personeel.ToList()
                                               where s.RolId == 3
                                               select new { Id = s.Id, FullName = s.Voornaam + " " + s.Naam }), "Id", "FullName", null); 

            ViewData["KokB"] = new SelectList((from s in _context.Personeel.ToList()
                                               where s.RolId == 3
                                               select new { Id = s.Id, FullName = s.Voornaam + " " + s.Naam }), "Id", "FullName", null); 

            return View();
        }

        // POST: KoksOnDuties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(KokInplanningModel model)
        {   
            if (ModelState.IsValid)
            {
                TblKoksOnDuty koks = new();
               
                koks.Id = model.Id;
                koks.KokA = model.KokA;
                koks.KokB = model.KokB;
               //koks.TijdSloten = tblKoksOnDuty.TijdSloten;
                koks.Datum = model.Datum.Date;
                koks.ShiftStart = model.ShiftStart;
                koks.ShiftStop =  model.ShiftStop;

                //save koks in tbl KoksOnDuty
                _context.Add(koks);
               await  _context.SaveChangesAsync();

                ////time--------------------
                ////https://stackoverflow.com/questions/39451342/how-to-increment-time-in-term-of-minutes
                int tijdslotInterval = _context.Instellingen.FirstOrDefault().TijdslotInterval;

                List<DateTime> slots = SetTimeSlots(model.Datum.Date, tijdslotInterval, model.ShiftStart,model.ShiftStop);
                foreach(DateTime item in slots)
                {
                    int id =  (_context.KoksOnDuties.ToList().OrderByDescending(s => s.Id).FirstOrDefault()).Id;
                    TblTijdSloten slot = new TblTijdSloten();
                    slot.KoksOnDutyId = id;
                    slot.KokA = model.KokA;
                    slot.KokB = model.KokB;
                    slot.Tijdslot = item;
                    _context.Add(slot);
                }
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        public List<DateTime> SetTimeSlots(DateTime date, int increment, DateTime shiftStart, DateTime shiftEnd)
        {
            int starthour = shiftStart.Hour;
            int startMin = shiftStart.Minute;
            int endHour = shiftEnd.Hour;
            int endMin =  shiftEnd.Minute;

            int inc = increment;
            DateTime startTime = date.AddHours(starthour).AddMinutes(startMin);
            DateTime endTime =   date.AddHours(endHour).AddMinutes(endMin);
            List<DateTime> timeList = new List<DateTime>();
            var timeSpan = endTime - startTime;
            while (startTime < endTime.AddMinutes(inc))
            {
                timeList.Add(startTime);
                startTime = startTime.AddMinutes(inc);
            }


            return timeList;
        }


        // GET: KoksOnDuties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kok = await _context.KoksOnDuties.FindAsync(id);

            if (kok == null)
            {
                return NotFound();
            }

            KokInplanningModel model = new();
            model.Id = kok.Id;

            model.Datum = kok.Datum;
            model.ShiftStart = kok.ShiftStart;
            model.ShiftStop = kok.ShiftStop;

            ViewData["KokAEdit"] = new SelectList((from s in _context.Personeel.ToList()
                                               where s.RolId == 3
                                               select new { Id = s.Id, FullName = s.Voornaam + " " + s.Naam }), "Id", "FullName",kok.KokA );

            ViewData["KokBEdit"] = new SelectList((from s in _context.Personeel.ToList()
                                               where s.RolId == 3
                                               select new { Id = s.Id, FullName = s.Voornaam + " " + s.Naam }), "Id", "FullName",kok.KokB );


            return View(model);
        }

        // POST: KoksOnDuties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  IActionResult Edit(int id, KokInplanningModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                var kok = _context.KoksOnDuties.Where(s => s.Id == id).FirstOrDefault();

                //A1> vergelijk datum en tijd van de shifts
                //   als deze niet gelijk is moeten de tijdsloten gewist worden en 
                //   de bestellingen verplaatst worden naar een ander tijdslot
                //----------------------------------------

                DateTime shiftStartOld = kok.ShiftStart;
                DateTime shiftStartNew = model.ShiftStart;
                DateTime shiftStopOld = kok.ShiftStop;
                DateTime shiftStopNew = model.ShiftStop;
                DateTime datumOld = kok.Datum;
                DateTime datumNew = model.Datum;

                if (shiftStartNew != shiftStartOld || datumNew != datumOld)
                {

                    if (datumNew != datumOld) { kok.Datum = datumNew; }
                    else { kok.Datum = datumOld; }

                    kok.Id = id;
                    kok.KokA = model.KokA;
                    kok.KokB = model.KokB;

                    kok.ShiftStart = model.ShiftStart;
                    kok.ShiftStop = model.ShiftStop;

                    _context.Update(kok);
                    _context.SaveChanges();

                    //B> zie of er bezette tijdsloten zijn en sla hun bestellingsid's op en delete de oude tijdsloten
                    //--------------------------------------------------------------------
                    var temp = _context.TijdSloten.Where(s => s.Tijdslot.Date == model.Datum).ToList();

                    List<int?> BestellingIdsTeWijzigen = new();
                 
                    foreach (var item in temp)
                    {         
                            //***
                            TimeSpan start = new TimeSpan(shiftStartOld.Hour, shiftStartOld.Minute, 0);
                            TimeSpan end = new TimeSpan(shiftStopOld.Hour, shiftStopOld.Minute, 0);
                            TimeSpan test = new TimeSpan(item.Tijdslot.Hour, item.Tijdslot.Minute, 0);

                            if (start <= end)
                            {
                                if (item.slotAChecked == true)
                                {
                                    BestellingIdsTeWijzigen.Add(item.BestellingA);
                                }
                                if (item.slotBChecked == true)
                                {
                                    BestellingIdsTeWijzigen.Add(item.BestellingB);
                                }
                                //Delete tijdslots
                                _context.TijdSloten.Remove(item);
                                _context.SaveChanges();
                            }

                    }



                    //C> maak nieuwe tijdslots aan
                    ////https://stackoverflow.com/questions/39451342/how-to-increment-time-in-term-of-minutes
                    int tijdslotInterval = _context.Instellingen.FirstOrDefault().TijdslotInterval;

                    List<DateTime> slots = SetTimeSlots(model.Datum.Date, tijdslotInterval, model.ShiftStart, model.ShiftStop);
                    foreach (DateTime item in slots)
                    {
                        int kokOnDutyid = (_context.KoksOnDuties.ToList().OrderByDescending(s => s.Id).FirstOrDefault()).Id;
                        TblTijdSloten slot = new TblTijdSloten();
                        slot.KoksOnDutyId = kokOnDutyid;
                        slot.KokA = model.KokA;
                        slot.KokB = model.KokB;
                        slot.Tijdslot = item;
                        _context.Add(slot);
                    }
                    _context.SaveChanges();

                    //D> Wijzig de bestellingen hun tijdslots
                    List<BestellingWijzigingen> bestellingenTeWijzigenLijst = new();
                    foreach (var item in BestellingIdsTeWijzigen)
                    {
                        BestellingWijzigingen bestelling = new();
                        var bestelData = _context.Bestellingen.Where(s => s.Id == item).FirstOrDefault();
                        var klantData = _context.Klanten.Where(s => s.Id == bestelData.KlantId).FirstOrDefault();
                        var klantDetails = _context.KlantDetails.Where(s => s.Id == klantData.KlantDetailsId).FirstOrDefault();

                        bestelling.Id = (int)item;
                        bestelling.Klant = klantData.Voornaam + " " + klantData.Naam;
                        bestelling.Telefoon = klantDetails.Telefoon;

                        bestellingenTeWijzigenLijst.Add(bestelling);

                    }
                    //D> Laat alleen de be bestellingen zien als er effectief bestellingen moeten gewijzigd worden
                    if (bestellingenTeWijzigenLijst.Count > 0)
                    {
                        return View("IndexWijzigingen", bestellingenTeWijzigenLijst); //todo juiste view laden!!!
                    }
                    else
                    {
                        return RedirectToAction(nameof(Index));
                    }

                }
                //A2> tijdsloten blijven ongewijzigd en alleen de koks veranderen
                else
                {
                    //TblKoksOnDuty
                    kok.Id = id;
                    kok.KokA = model.KokA;
                    kok.KokB = model.KokB;
                    kok.Datum = model.Datum;
                    kok.ShiftStart = model.ShiftStart;
                    kok.ShiftStop = model.ShiftStop;

                    _context.Update(kok);
                    _context.SaveChanges();

                    //TblTijdloten update
                    var tSlots = _context.TijdSloten.Where(s => s.KoksOnDutyId == model.Id).ToList();
                    foreach(var item in tSlots)
                    {
                        item.KokA = model.KokA;
                        item.KokB = model.KokB;
                        _context.Update(item);
                        _context.SaveChanges();
                    }


                    return RedirectToAction(nameof(Index));
                }
                
            }
            return RedirectToAction(nameof(Index));
        }
        //BestelWijzigingen index
        public IActionResult IndexWijzigingen()
        {
            return View();
        }

        // GET: KoksOnDuties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kok = await _context.KoksOnDuties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (kok == null)
            {
                return NotFound();
            }

            KokInplanningModel model = new();
            model.Id = kok.Id;
            var naamA = _context.Personeel.Where(s => s.Id == kok.KokA).FirstOrDefault();
            var naamB = _context.Personeel.Where(s => s.Id == kok.KokB).FirstOrDefault();
            model.KokANaam = naamA.Voornaam + " " + naamA.Naam;

            model.KokBNaam = naamB.Voornaam + " " + naamB.Naam;

            model.Datum = kok.Datum;
            model.ShiftStart = kok.ShiftStart;
            model.ShiftStop = kok.ShiftStop;

            return View(model);
        }

        // POST: KoksOnDuties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblKoksOnDuty = await _context.KoksOnDuties.FindAsync(id);
            _context.KoksOnDuties.Remove(tblKoksOnDuty);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblKoksOnDutyExists(int id)
        {
            return _context.KoksOnDuties.Any(e => e.Id == id);
        }
    }
}
