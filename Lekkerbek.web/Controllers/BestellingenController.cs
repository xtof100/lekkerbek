﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;
using System.Net.Mail;


namespace Lekkerbek.web.Controllers
{
    public class BestellingenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public BestellingenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Bestellingen
        public  IActionResult Index()
        {
            //A> Data uit database
            //-----------------------
            var bestellingsData = _context.Bestellingen.Include(t => t.Klant);

            //B> Vul lijst met bestellingsmodels
            //-------------------------------------
            List <BestellingOverzichtModel> overzicht = new();
            foreach(var item in bestellingsData)
            {
                BestellingOverzichtModel model = new();
               

                var naam = (from s in _context.Bestellingen
                                 join c in _context.Klanten
                                  on s.KlantId equals c.Id
                                 where s.Id == item.Id
                                 select new { VolledigeNaam = c.Voornaam + ' ' + c.Naam }).FirstOrDefault();
                if (naam != null) { model.KlantNaam = naam.VolledigeNaam; }
                    
                model.AfhaalTijdstip = item.AfhaalTijdstip;
                model.AantalMaaltijden = item.AantalMaaltijden;
                    var PrijsGerechten = from s in _context.Bestellingen
                                         join c in _context.BesteldeGerechten
                                         on s.Id equals c.BestellingId
                                         join d in _context.Gerechten
                                         on c.GerechtenId equals d.Id
                                         where s.Id == item.Id
                                         select d.Prijs;
                    float totalePrijs = 0;
                    foreach(var i in PrijsGerechten) { totalePrijs += i; }
                    var PrijsOpties =    from s in _context.Bestellingen
                                         join c in _context.BesteldeGerechten
                                         on s.Id equals c.BestellingId
                                         join d in _context.GerechtOpties
                                         on c.GerechtOptiesId equals d.Id
                                         where s.Id == item.Id
                                         select d.Prijs;
                    float totaleOptiesPrijs = 0;


                    foreach (var j in PrijsOpties) { totaleOptiesPrijs += j; }


                //prijs berekening
                //----------------------
                     float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;
                     model.TotalePrijs =(float) ((totalePrijs + totaleOptiesPrijs) * btw);

                model.Id = item.Id;

                //B1>check of bestelling bestaat in facturatie tabel zoniet moet de knop afrekenen verschijnen in index
                //------------------------------------------------------------------------------------------------------
                var facturatieAfgehandeld =(bool) (from s in _context.Facturaties
                                            join t in _context.Afgehandeld
                                            on s.Id equals t.FacturatieId
                                            where s.Id == item.Id
                                            select t.Afgehandeld).FirstOrDefault();

                if (facturatieAfgehandeld == true)
                {
                    model.Betaald = (bool)(from s in _context.Facturaties
                                           join t in _context.Afgehandeld
                                           on s.Id equals t.FacturatieId
                                           where s.Id == item.Id
                                           select t.Afgehandeld).FirstOrDefault(); 
                }
                
                else 
                {
                    model.Betaald = false;
                }
                overzicht.Add(model);
            }

            //C> Set layout en andere viewbags
            //--------------
            ViewBag.LoginData = _context.LoginData.FirstOrDefault();
            

            return View(overzicht);
        }

       // GET: Bestellingen/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //a> Opvragen van de nodige data uit de database
            //----------------------------------------------
            var bestelling = _context.Bestellingen.Find(id);

            var gerechten = (from s in _context.BesteldeGerechten
                             join c in _context.Gerechten
                              on s.GerechtenId equals c.Id
                             where s.BestellingId == id
                             select new { c.GerechtNaam, c.Prijs }).ToList();

            var gerechtOpties = (from s in _context.BesteldeGerechten
                                 join c in _context.GerechtOpties
                                  on s.GerechtOptiesId equals c.Id
                                 where s.BestellingId == id
                                 select new { c.Optie, c.Prijs }).ToList();

            var klantInfo = (from s in _context.Bestellingen
                             join c in _context.Klanten
                              on s.KlantId equals c.Id
                             where s.Id == id
                             select new { VolledigeNaam = c.Voornaam + ' ' + c.Naam, c.Adres, c.KlantDetailsId, c.Getrouwheidscore }).FirstOrDefault();

            var klantInfoDetails = _context.KlantDetails.Where(s => s.Id == klantInfo.KlantDetailsId).FirstOrDefault();


            //B> Maak een lijst met gerechten
            //---------------------------------
            List<GerechtenModel> gerechtenLijst = new();

            for (int i = 0; i < gerechten.Count; i++)
            {
                GerechtenModel model = new()
                {
                    GerechtNaam = gerechten[i].GerechtNaam,
                    GerechtPrijs = gerechten[i].Prijs,
                    Optie = gerechtOpties[i].Optie,
                    OptiePrijs = gerechtOpties[i].Prijs

                };

                gerechtenLijst.Add(model);
            }


            //C> Vul het factuurModel in
            //----------------------------

            FactuurModel details = new()
            {
                //bestelling
                Id = (int)id,
                GerechtenLijst = gerechtenLijst,
                AantalMaaltijden = gerechten.Count,
                AfhaalTijdstip = bestelling.AfhaalTijdstip,

                //klant
                KlantNaam = klantInfo.VolledigeNaam,
                Adres = klantInfo.Adres,
                Straat = klantInfoDetails.Straat,
                HuisNr = klantInfoDetails.Huisnr,
                Postcode = klantInfoDetails.Postcode,
                Stad = klantInfoDetails.Stad,
                Email = klantInfoDetails.Email,
                Telefoon = klantInfoDetails.Telefoon,

                //layout
                Layout = _context.LoginData.FirstOrDefault().Layout

            };

            //C> Berekening van het verschuldigd bedrag
            //-------------------------------------------------------------------------------
            float totaalPrijsZonderBtw = 0;
            float totaalPrijsMetBtw = 0;
            float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;

            for (int i = 0; i < gerechten.Count; i++)
            {
                totaalPrijsZonderBtw += gerechten[i].Prijs;
                totaalPrijsZonderBtw += gerechtOpties[i].Prijs;
            }

            totaalPrijsMetBtw = (float)(totaalPrijsZonderBtw  * btw);
            ViewData["TotaalPrijs"] = totaalPrijsMetBtw;

            //D>Set layout
            //--------------
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;
           
            return View(details);

        }

       
        public IActionResult Factuur(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //a> Opvragen van de nodige data uit de database
            //----------------------------------------------
            var bestelling =  _context.Bestellingen.Find(id);

            var gerechten = (from s in _context.BesteldeGerechten
                             join c in _context.Gerechten
                              on s.GerechtenId equals c.Id
                             where s.BestellingId == id
                             select new { c.GerechtNaam, c.Prijs }).ToList();

            var gerechtOpties = (from s in _context.BesteldeGerechten
                                 join c in _context.GerechtOpties
                                  on s.GerechtOptiesId equals c.Id
                                 where s.BestellingId == id
                                 select new { c.Optie, c.Prijs }).ToList();
 
            var klantInfo = (from s in _context.Bestellingen
                             join c in _context.Klanten
                              on s.KlantId equals c.Id
                             where s.Id == id
                             select new { VolledigeNaam = c.Voornaam + ' ' + c.Naam, c.Adres, c.KlantDetailsId, c.Getrouwheidscore}).FirstOrDefault();

            var klantInfoDetails = _context.KlantDetails.Where(s => s.Id == klantInfo.KlantDetailsId).FirstOrDefault();

            var bedrijfsGegevens = _context.Instellingen.FirstOrDefault();

            //B> Maak een lijst met gerechten
            //---------------------------------
            List<GerechtenModel> gerechtenLijst = new();

            for (int i= 0; i < gerechten.Count; i++)
            {
                GerechtenModel model = new()
                { 
                    GerechtNaam = gerechten[i].GerechtNaam, 
                    GerechtPrijs = gerechten[i].Prijs,
                    Optie = gerechtOpties[i].Optie,
                    OptiePrijs = gerechtOpties[i].Prijs
                
                };

                gerechtenLijst.Add(model);
            }


            //C> Vul het factuurModel in
            //----------------------------
            FactuurModel factuur = new()
            {
                //bestelling
                Id =(int) id ,
                GerechtenLijst = gerechtenLijst,
                AantalMaaltijden = gerechten.Count,
                AfhaalTijdstip = bestelling.AfhaalTijdstip,

                //klant
                KlantNaam = klantInfo.VolledigeNaam,
                Adres = klantInfo.Adres,
                Straat = klantInfoDetails.Straat,
                HuisNr = klantInfoDetails.Huisnr,
                Postcode = klantInfoDetails.Postcode,
                Stad = klantInfoDetails.Stad,
                Email = klantInfoDetails.Email,
                Telefoon = klantInfoDetails.Telefoon,
                
                Betaalwijze = "card/cash",

                //bedrijf
                BedrijfNaam = bedrijfsGegevens.Naam,
                BedrijfStraat = bedrijfsGegevens.Straat,
                BedrijfHuisNr = bedrijfsGegevens.Huisnr,
                BedrijfPostcode = bedrijfsGegevens.Postcode,
                BedrijfStad = bedrijfsGegevens.Stad,
                BedrijfEmail = bedrijfsGegevens.Email,
                BedrijfTelefoon = bedrijfsGegevens.Telefoon

            };


            //C> Berekening van het verschuldigd bedrag
            //-------------------------------------------------------------------------------
            float totaalPrijsZonderBtw = 0;
            float totaalPrijsMetBtw = 0;
            float btwBedrag = 0;
            float kortingBedrag = 0;
            float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;
            float korting = _context.Instellingen.FirstOrDefault().KortingWaardeInDecimaal;

            for (int i = 0; i < gerechten.Count; i++)
            {
                totaalPrijsZonderBtw += gerechten[i].Prijs;
                totaalPrijsZonderBtw += gerechtOpties[i].Prijs;             
            }

                //C1> Check of de klant recht heeft op een korting
                if(klantInfo.Getrouwheidscore >= 3)
                {
                    kortingBedrag = (float)(totaalPrijsZonderBtw * korting); //10% korting na 3 bestellingen getrouwheidscore
                }
            
            totaalPrijsMetBtw = (float)((totaalPrijsZonderBtw - korting) * btw);
            btw = (float)Math.Round(totaalPrijsMetBtw - totaalPrijsZonderBtw, 2);

            ViewData["TotaalPrijsZonderBtw"] = totaalPrijsZonderBtw; 
            ViewData["TotaalPrijsMetBtw"] = totaalPrijsMetBtw; 
            ViewData["BtwBedrag"] = btwBedrag; 
            ViewData["KortingBedrag"] = korting; 
            ViewData["Korting"] = false;


            //D>Set layout
            //--------------
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;


            return View(factuur);

        }


        public IActionResult EmailFactuur(int? id)
        {
            //a> get model data
            //------------------
            var bestelling = _context.Bestellingen.Find(id);

            var gerechten = (from s in _context.BesteldeGerechten
                             join c in _context.Gerechten
                              on s.GerechtenId equals c.Id
                             where s.BestellingId == id
                             select new { c.GerechtNaam, c.Prijs }).ToList();

            var gerechtOpties = (from s in _context.BesteldeGerechten
                                 join c in _context.GerechtOpties
                                  on s.GerechtOptiesId equals c.Id
                                 where s.BestellingId == id
                                 select new { c.Optie, c.Prijs }).ToList();

            var klantInfo = (from s in _context.Bestellingen
                             join c in _context.Klanten
                              on s.KlantId equals c.Id
                             where s.Id == id
                             select new { VolledigeNaam = c.Voornaam + ' ' + c.Naam, c.Adres, c.KlantDetailsId, c.Getrouwheidscore }).FirstOrDefault();

            var klantInfoDetails = _context.KlantDetails.Where(s => s.Id == klantInfo.KlantDetailsId).FirstOrDefault();

            var bedrijfsGegevens = _context.Instellingen.FirstOrDefault();

            var facturatie = (from s in _context.Facturaties
                              join t in _context.Afgehandeld
                              on s.Id equals t.FacturatieId
                              join u in _context.BetaalWijzen
                              on t.BetaalWijzeId equals u.Id
                              where s.Id == id
                              select new { u.BetaalWijze }).FirstOrDefault();


            //prijsberekeningen
            //C> Berekening van het verschuldigd bedrag
            //-------------------------------------------------------------------------------
            float totaalPrijsZonderBtw = 0;
            float totaalPrijsMetBtw = 0;
            float btwBedrag = 0;
            float kortingBedrag = 0;
            float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;
            float korting = _context.Instellingen.FirstOrDefault().KortingWaardeInDecimaal;
            int getrouwheidsLimiet = _context.Instellingen.FirstOrDefault().GetrouwheidScoreLimiet;

            for (int j = 0; j < gerechten.Count; j++)
            {
                totaalPrijsZonderBtw += gerechten[j].Prijs;
                totaalPrijsZonderBtw += gerechtOpties[j].Prijs;
            }

            totaalPrijsZonderBtw = (float)Math.Round(totaalPrijsZonderBtw, 2);

                //C1> Check of de klant recht heeft op een korting
                if (klantInfo.Getrouwheidscore >= getrouwheidsLimiet)
                {
                    kortingBedrag = (float)Math.Round((totaalPrijsZonderBtw * korting),2); // korting na 3 bestellingen getrouwheidscore in tegeven bij de instellingen
                }

            totaalPrijsMetBtw = (float)Math.Round(((totaalPrijsZonderBtw - korting) * btw),2);
            btwBedrag = (float)Math.Round((totaalPrijsMetBtw - totaalPrijsZonderBtw),2);


            //B> Maak html string
            //-------------------
           


            string body = "";
            body += " <!DOCTYPE html>";
            body += "<html>";
            body += "<head> <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>";
            body += "<style>";
            body += "body {margin - top: 20px; background: #eee;}";            
            body += ".invoice {padding: 30px;}";
            body += ".invoice h2 { margin - top: 0px; line - height: 0.8em;}";
            body += ".invoice.small {font - weight: 300;}";
            body += " .invoice hr {margin - top: 10px;border - color: #ddd;}";
            body += ".invoice.table tr.line {border - bottom: 1px solid #ccc;}";
            body += ".invoice.table td { border: none; }";
            body += ".invoice.identity { margin - top: 10px;font - size: 1.1em;font - weight: 300;}";
            body += ".invoice.identity strong { font - weight: 600;}";
            body += ".grid { position: relative; width: 100 %; background: #fff;  color: #666666; border - radius: 2px; margin - bottom: 25px;  box - shadow: 0px 1px 4px rgba(0, 0, 0, 0.1); }";
            body += " </style>";
            body += " </head> ";

            body += "<body>";           
            body += "<div class='col-xs-12'>";
            body += "<div class='grid-body'>";
            body += "<div class='invoice - title'>";
            body += "<div class='row'>";
            body += "<div class='col-xs-12'>";
            body += "<img src = '/img/logo.png'  alt='Logo'  height='35'>";           
            body += "</div></div><br/>";
            body += "<div class='row'>";
            body += "<div class='col-xs-12'>";
            body += "<h2>Factuur</h2><br/>";
            body += "<span class='small'>"+bestelling.Id+"</span>";
            body += "</div></div></div><hr>";
            body += "<div class='row'>";
            body += "<div class='col-6'>";
            body += "<address>";
            body += "<strong>Facturatie Adres:</strong><br/>";
            body +=  klantInfo.VolledigeNaam + "<br/>";
            body +=  klantInfoDetails.Straat  + " " + klantInfoDetails.Huisnr + "<br/>";
            body +=  klantInfoDetails.Postcode + " " + klantInfoDetails.Stad + "<br/>";
            body += "<abbr title = 'Telefoon'> T:</abbr> " + klantInfoDetails.Telefoon;
            body += "</address>";
            body += "</div>";
            body += "<div class='col-6 text-right'>";
            body += "<address>";
            body += "<strong>BedrijfsAdres:</strong><br/>";
            body +=  bedrijfsGegevens.Naam + "<br/>";
            body +=  bedrijfsGegevens.Straat + " " +  bedrijfsGegevens.Huisnr + "<br/>";
            body +=  bedrijfsGegevens.Postcode + " " + bedrijfsGegevens.Stad + "<br/>";
            body += "<abbr title = 'Telefoon' > T:</abbr> " + bedrijfsGegevens.Telefoon + "<br/>";
            body += "<abbr title = 'Email' > E:</abbr> " + bedrijfsGegevens.Email + "<br/>";
            body += "</address>";
            body += "</div></div>";
            body += "<div class='row'>";
            body += "<div class='col-6'>";
            body += "<address>";
            body += "<strong>Betaalwijze: </strong><br/>";
            body += facturatie.BetaalWijze +  "<br/>";
           
            body += "</address>";
            body += "</div>";
            body += "<div class='col-6 text-right'>";
            body += "<address>";
            body += "<strong>Afhaaltijdstip:</strong><br/>";
            body += bestelling.AfhaalTijdstip.ToShortDateString() + "<br/>";
            body += bestelling.AfhaalTijdstip.ToShortTimeString() + "<br/>";
            body += "</address>";
            body += "</div></div>";
            body += "<div class='row'>";
            body += "<div class='col-md-12'>";
            body += "<h3>BESTELLINGSOVERZICHT</h3>";
            body += "<table class='table table-striped'>";
            body += "<thead>";
            body += "<tr class='line'>";
            body += "<td><strong>#</strong></td>";
            body += "<td class='text-center'><strong>GERECHT</strong></td>";
            body += "<td class='text-center'><strong>OPTIE</strong></td>";
            body += "<td class='text-right'><strong>PRIJS GERECHT</strong></td>";
            body += "<td class='text-right'><strong>PRIJS OPTIE</strong></td>";
            body += "</tr>";
            body += "</thead>";
            body += "<tbody>";

            
            int i = 0;
            foreach (var item in gerechten)
            {
                body += "<tr>";
                body += " <td>"+ i +  "</td>";
                body += "<td>" + item.GerechtNaam +"</td>";
                body += "<td>" + gerechtOpties[i].Optie + "</td>";
                body += "<td class='text-right'>" + item.Prijs + "€ </td>";
                body += "<td class='text-right'>" + gerechtOpties[i].Prijs + "€ </td>";
                body += "</tr>";
                
                i ++;
            }


            body += "<tr>";
            body += "<td colspan = '3'></td>";
            body += "<td class='text-right'><strong>Totaal Exclusief 21% BTW</strong></td>";
            body += "<td class='text-right'><strong>"+ totaalPrijsZonderBtw + "€</strong></td>"; 
            body += "</tr>";

            if (klantInfo.Getrouwheidscore >= getrouwheidsLimiet)
            {
                body += "<tr>";
                body += "<td colspan = '3' ></ td>";
                body += "<td class='text-right'><strong>Klant Korting</strong></td>";
                body += "<td class='text-right'><strong>"+ kortingBedrag + " €</strong></td>";
                body += "</tr>";
            }

            body += " <tr>";
            body += " <td colspan = '3' ></ td>";
            body += "<td class= 'text-right'><strong> 21 % BTW </strong></td>";
            body += "<td class= 'text-right'><strong> "+ btwBedrag +"€</strong></td>";
            body += "</tr>";
            body += "<tr>";
            body += "<td colspan = '3'>";
            body += "</td><td class= 'text-right'><strong> + Totaal inclusief BTW</strong></td>";
            body += "<td class= 'text-right'><strong>"+ totaalPrijsMetBtw + " € </strong></td>";
            body += "</tr>";
            body += "</tbody>";
            body += "</table>";
            body += "</div ></div>";
            body += "<div class= 'row' >";
            body += "<div class= 'col-md-12 text-right identity'>";
            body += "<p><strong> De lekkerbek dankt je voor het vertrouwen</strong></p>";
            body += "</div></div></div></div></div></div>";
            

            body += "</html>";
            body += "</body>";

            //C> Email string 
            //-------------------

            MailMessage mail = new MailMessage();
            mail.To.Add(klantInfoDetails.Email);       //klant email
            mail.From = new MailAddress(bedrijfsGegevens.AfzenderAdres); //Lekkerbek email
            mail.Subject = bedrijfsGegevens.Onderwerp;
          
            mail.IsBodyHtml = true;
            mail.Body = body; //mail inner data
            

            SmtpClient smtp = new SmtpClient();
            smtp.Host = bedrijfsGegevens.SmtpHost; //smpt server
            smtp.Port = bedrijfsGegevens.SmtpPort; //smtp port
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(bedrijfsGegevens.Login, bedrijfsGegevens.Paswoord); // Enter seders User name and password  
            smtp.EnableSsl = true;
            smtp.Send(mail);
           
            //confirmatie msg

            return RedirectToAction(nameof(EmailConfirmMsg));
        }

        public IActionResult EmailConfirmMsg()
        {
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            return View();
        }

        public IActionResult Create()
        {
            //A> check of er koks ingepland zijn en zoek eerste vrije tijdslot in de toekomst +2u3O!
            //---------------------------------------------------------------------------------------
            var koksIngepland = _context.KoksOnDuties.Select(s => s.Datum).ToList();

            if (koksIngepland.Count != 0)
            {

                //A1> get de tijdsloten in de toekomst(min 2uur) die nog vrij zijn 
                var nu = DateTime.Now;
                var uur = nu.Hour + 2;
                var dag = nu.Day;

                //check voor middernacht
                if (nu.Hour + 2 >= 24)
                {
                    uur = 0;
                    dag = nu.Day + 1;
                }

                var hm = new DateTime(nu.Year, nu.Month, dag, uur, nu.Minute, 0);
                var ts = _context.TijdSloten.Where(s => s.Tijdslot > hm).ToList();

                //A2> kies eerst volgend vrij slot
                var tsVrijA = ts.Where(s => s.slotAChecked == false).FirstOrDefault();
                var tsVrijB = ts.Where(s => s.slotBChecked == false).FirstOrDefault();
                DateTime tsResult = DateTime.Now;

                
                if(tsVrijA == null && tsVrijB == null)// verwittig gebruiker als er geen koks ingepland zijn
                {
                    ModelState.AddModelError("geen_koks", "Er zijn nog geen koks in de database");
                    return RedirectToAction("GeenKoksMsg");
                }
                else
                {

                    //B> create BestellingId 
                    //------------------------------

                    if (tsVrijA.Tijdslot > tsVrijB.Tijdslot)
                    {
                        //B1> get tijdslot en steek dit in een object bestellingen
                        tsResult = tsVrijB.Tijdslot;
                        
                        TblBestelling tblBestelling = new()
                        {
                            AfhaalTijdstip = tsResult
                        };

                        //B2> save zodat er een id word aangemaakt in bestellingen
                        _context.Add(tblBestelling);
                        _context.SaveChanges();

                        //B3> Roep de aangemaakte bestelling op zodat de id kan opgeslagen worden in de tabel van de tijdsloten
                        var bestellingsQuery = _context.Bestellingen.ToList();
                        var bestellingsQueryOrderd = bestellingsQuery.OrderByDescending(s => s.Id).FirstOrDefault();

                        tsVrijB.slotBChecked = true;
                        tsVrijB.BestellingB = bestellingsQueryOrderd.Id;
                       

                        _context.Update(tsVrijB);
                        _context.SaveChanges();

                        //B4> preparee het bestellingsmodel
                        BestellingModel bestelling = new()
                        {
                            Id = bestellingsQueryOrderd.Id,
                            KlantId = bestellingsQueryOrderd.KlantId,
                            //var src2 = DateTime.Now;
                            // var hm2 = new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);
                            AfhaalTijdstip = tsResult,
                            AantalMaaltijden = 0,
                            Layout = _context.LoginData.FirstOrDefault().Layout
                        };

                        //C> selectbox voor datums
                        //----------------------------
                            var query = _context.TijdSloten.ToList();
                            //filter datums uit het verleden eruit en selecteer de distinct value
                            var datumSelectie = query.Where(s => s.Tijdslot.Date >= DateTime.Today.Date).DistinctBy(s => s.Tijdslot.Date).Select(d => new { d.Id, datum = d.Tijdslot.ToShortDateString() }); //opgelet!! distinct by alleen vanaf .net6

                            ViewData["DatumSelectie"] = new SelectList(datumSelectie, "datum", "datum");


                            //klanten selectbox
                            ViewData["KlantId"] = new SelectList((from s in _context.Klanten.ToList()
                                                                  select new { s.Id, FullName = s.Voornaam + ' ' + s.Naam }), "Id", "FullName", null);

                            //Layout en rol data
                            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
                            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;


                        return View(bestelling);

                    }
                    else
                    {
                        //B1> get tijdslot en steek dit in een object bestellingen
                        tsResult = tsVrijA.Tijdslot;

                        TblBestelling tblBestelling = new()
                        {
                            AfhaalTijdstip = tsResult
                        };

                        //B2> save zodat er een id word aangemaakt in bestellingen
                        _context.Add(tblBestelling);
                        _context.SaveChanges();

                        //B3> Roep de aangemaakte bestelling op zodat de id kan opgeslagen worden in de tabel van de tijdsloten
                        var bestellingsQuery = _context.Bestellingen.ToList();
                        var bestellingsQueryOrderd = bestellingsQuery.OrderByDescending(s => s.Id).FirstOrDefault();

                        tsVrijB.slotAChecked = true;
                        tsVrijB.BestellingA = bestellingsQueryOrderd.Id;
                       

                        _context.Update(tsVrijA);
                        _context.SaveChanges();

                        //B4> preparee het bestellingsmodel
                        BestellingModel bestelling = new()
                        {
                            Id = bestellingsQueryOrderd.Id,
                            KlantId = bestellingsQueryOrderd.KlantId,
                            //var src2 = DateTime.Now;
                            // var hm2 = new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);
                            AfhaalTijdstip = tsResult,
                            AantalMaaltijden = 0,
                            Layout = _context.LoginData.FirstOrDefault().Layout
                    };

                        //C> selectbox voor datums
                        //----------------------------
                            var query = _context.TijdSloten.ToList();
                            //filter datums uit het verleden eruit en selecteer de distinct value
                            var datumSelectie = query.Where(s => s.Tijdslot.Date >= DateTime.Today.Date).DistinctBy(s => s.Tijdslot.Date).Select(d => new { d.Id, datum = d.Tijdslot.ToShortDateString() }); //opgelet!! distinct by alleen vanaf .net6

                            ViewData["DatumSelectie"] = new SelectList(datumSelectie, "datum", "datum");


                            //klanten selectbox
                            ViewData["KlantId"] = new SelectList((from s in _context.Klanten.ToList()
                                                                  select new { s.Id, FullName = s.Voornaam + ' ' + s.Naam }), "Id", "FullName", null);

                        //Layout en rol data
                       
                        ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;


                        return View(bestelling);

                    }

                    
                }

            }
            else
            { 
                ModelState.AddModelError("geen_koks", "Er zijn nog geen koks in de database");
                return RedirectToAction("GeenKoksMsg");
            }
     
        }

        // POST: Bestellingen/Create
       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(BestellingModel model)
        {
            if (ModelState.IsValid)
            {
                var besteldeGerechtenList = _context.Gerechten.ToList();
                TblBestelling result = new()
                {
                    Id = model.Id,
                    KlantId = model.KlantId,
                    AfhaalTijdstip = model.AfhaalTijdstip,
                    AantalMaaltijden = besteldeGerechtenList.OrderByDescending(s => s.Id == model.Id).Count()
                };

                //steek bestellings id in tijdslot
                var tblTijdSloten = _context.TijdSloten.Where(s => s.Tijdslot == model.AfhaalTijdstip).FirstOrDefault();
                if (tblTijdSloten.slotAChecked == true)
                {
                    tblTijdSloten.BestellingA = model.Id;
                }
                else
                {
                    tblTijdSloten.BestellingB = model.Id;
                }
                _context.Update(tblTijdSloten);
                _context.Update(result);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        public IActionResult GeenKoksMsg()
        {
            //Layout en rol data
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;

            return View();
        }

            [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddGerecht(BestellingModel model)
        {
            if (ModelState.IsValid)
            {
                TblBestelling result = new()
                {
                    Id = model.Id,
                    KlantId = model.KlantId,
                    AfhaalTijdstip = model.AfhaalTijdstip
                };

                //A> save bestelling
                //------------------------
                 _context.Update(result);
                 _context.SaveChanges();

                //B> save klant id in tijdslot
                //----------------------------
                var tijdslot = _context.TijdSloten.Where(s => s.BestellingA == result.Id || s.BestellingB == result.Id).FirstOrDefault();

                if(tijdslot.BestellingA == result.Id) { tijdslot.KlantA = result.KlantId; }
                else { tijdslot.KlantB = result.KlantId; }
                _context.Update(tijdslot);
                _context.SaveChanges();

                return RedirectToAction ("create","BesteldeGerechten");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> KiesTijdSlot(BestellingModel model,DateTime geselecteerdeDag)
        {
            if (ModelState.IsValid)
            {
                TblBestelling result = new()
                {
                    Id = model.Id,
                    KlantId = model.KlantId,
                    AfhaalTijdstip = model.AfhaalTijdstip
                };

                _context.Update(result);
                await _context.SaveChangesAsync();
                TempData["GekozenTijdslotDatum"] = geselecteerdeDag;
                TempData["Data"] = result.KlantId;
                TempData["Bestelling"] = result.Id;
                return Redirect("/TijdSloten/TijdSloten");
            }
            return View();
        }

        // GET: Bestellingen/Edit/5
        public IActionResult Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            else
            {

                //A> laad De betrokken tabbelen in
                //-----------------------------------

                var bestelling =     _context.Bestellingen.Find(id);

                var gerechten =     (from s in _context.BesteldeGerechten
                                     join c in _context.Gerechten
                                     on s.GerechtenId equals c.Id
                                     where s.BestellingId == id
                                     select new { c.GerechtNaam, c.Prijs }).ToList();

                var gerechtOpties = (from s in _context.BesteldeGerechten
                                     join c in _context.GerechtOpties
                                     on s.GerechtOptiesId equals c.Id
                                     where s.BestellingId == id
                                     select new { c.Optie, c.Prijs }).ToList();

                var klantInfo =     (from s in _context.Bestellingen
                                     join c in _context.Klanten
                                     on s.KlantId equals c.Id
                                     where s.Id == id
                                     select new { VolledigeNaam = c.Voornaam + ' ' + c.Naam, c.Adres,c.Id, c.KlantDetailsId, c.Getrouwheidscore }).FirstOrDefault();

                var klantInfoDetails = _context.KlantDetails.Where(s => s.Id == klantInfo.KlantDetailsId).FirstOrDefault();

                var bedrijfsGegevens = _context.Instellingen.FirstOrDefault();

                //B> Vul het factuurmodel met de juiste gegevens en slaag dit op in een lijst voor elke bestelling
                //--------------------------------------------------------------------------------------------------
                //B> Maak een lijst met gerechten
                //---------------------------------
                List<GerechtenModel> gerechtenLijst = new();

                for (int i = 0; i < gerechten.Count; i++)
                {
                    GerechtenModel model = new()
                    {
                        GerechtNaam = gerechten[i].GerechtNaam,
                        GerechtPrijs = gerechten[i].Prijs,
                        Optie = gerechtOpties[i].Optie,
                        OptiePrijs = gerechtOpties[i].Prijs

                    };

                    gerechtenLijst.Add(model);
                }


                //C> Vul het factuurModel in
                //----------------------------
                FactuurModel edit = new()
                {
                    //bestelling
                    Id = (int)id,
                    GerechtenLijst = gerechtenLijst,
                    AantalMaaltijden = gerechten.Count,
                    AfhaalTijdstip = bestelling.AfhaalTijdstip,

                    //klant
                    KlantId = klantInfo.Id,
                    KlantNaam = klantInfo.VolledigeNaam,
                    Adres = klantInfo.Adres,
                    Straat = klantInfoDetails.Straat,
                    HuisNr = klantInfoDetails.Huisnr,
                    Postcode = klantInfoDetails.Postcode,
                    Stad = klantInfoDetails.Stad,
                    Email = klantInfoDetails.Email,
                    Telefoon = klantInfoDetails.Telefoon,

                    //layout
                    Layout = _context.LoginData.FirstOrDefault().Layout

                 };

                    //check of tijdslot veranderd is
                    if (TempData["GekozenTijdSlot"] != null)
                    {
                       edit.AfhaalTijdstip = (DateTime)TempData["GekozenTijdSlot"];                  
                    }


                //C> Berekening van het verschuldigd bedrag
                //-------------------------------------------------------------------------------
                float totaalPrijsZonderBtw = 0;
                float totaalPrijsMetBtw = 0;
                float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;

                for (int i = 0; i < gerechten.Count; i++)
                {
                    totaalPrijsZonderBtw += gerechten[i].Prijs;
                    totaalPrijsZonderBtw += gerechtOpties[i].Prijs;
                }
                totaalPrijsMetBtw = (float)(totaalPrijsZonderBtw * btw);

                ViewData["TotaalPrijs"] = totaalPrijsMetBtw;

                //D>ViewData voor Properties die niet in het model 
                //----------------------------------------------------------------------------------

                ViewData["KlantId"] = new SelectList((from s in _context.Klanten.ToList()
                                                      select new { s.Id, FullName = s.Voornaam + " " + s.Naam }), "Id", "FullName", null);
                    //selectbox voor datums
                    var query = _context.TijdSloten.ToList();
                    //filter datums uit het verleden eruit en selecteer de distinct value
                    var datumSelectie = query.Where(s => s.Tijdslot.Date >= DateTime.Today.Date).DistinctBy(s => s.Tijdslot.Date).Select(d => new { d.Id, datum = d.Tijdslot.ToShortDateString() }); //opgelet!! distinct by alleen vanaf .net6
                    ViewData["DatumSelectie"] = new SelectList(datumSelectie, "datum", "datum");


                //RolData
                ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;

                return View(edit);
            }
    
        }

        // POST: Bestellingen/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,KlantId,AfhaalTijdstip,AantalMaaltijden,BesteldeGerechtenId")] TblBestelling tblBestelling)
        {
            if (id != tblBestelling.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblBestelling);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblBestellingExists(tblBestelling.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["KlantId"] = new SelectList(_context.Klanten, "Id", "Id", tblBestelling.KlantId);
            return View(tblBestelling);
        }

        // GET: Bestellingen/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblBestelling = await _context.Bestellingen
                .Include(t => t.Klant)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblBestelling == null)
            {
                return NotFound();
            }

            return View(tblBestelling);
        }

        // POST: Bestellingen/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //wis tabel bestelling
            var tblBestelling = await _context.Bestellingen.FindAsync(id);
            _context.Bestellingen.Remove(tblBestelling);
           
            //wis tbale bestelde gerechten
            var tblBesteldeGerechten = _context.BesteldeGerechten.Where(s => s.BestellingId == id).ToList();
            foreach(var item in tblBesteldeGerechten)
            {
                _context.BesteldeGerechten.Remove(item);
            }
           
            //reset gekozen tijdslot 
            var tblTijdSloten = _context.TijdSloten.Where(s => s.Tijdslot == tblBestelling.AfhaalTijdstip).FirstOrDefault();
            if(tblTijdSloten.BestellingA == tblBestelling.Id)
            {
                tblTijdSloten.slotAChecked = false;
            }
            else 
            {
                tblTijdSloten.slotBChecked = false;
            }
            _context.Update(tblTijdSloten);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblBestellingExists(int id)
        {
            return _context.Bestellingen.Any(e => e.Id == id);
        }
  
    }
  
}
