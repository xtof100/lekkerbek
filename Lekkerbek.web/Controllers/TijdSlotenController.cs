﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class TijdSlotenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public TijdSlotenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: TijdSloten
        public IActionResult Index()
        {
            var data = _context.TijdSloten.Include(t => t.KoksOnDuty);
            List<TijdSlotenModel> tijdslotenOverzicht = new();

            foreach (var item in data)
            {
                if(item.Tijdslot> DateTime.Now)
                {
                    TijdSlotenModel model = new();
                    model.TijdSlot = item.Tijdslot;
                    //A-----
                    var klntA = (from s in _context.Klanten where s.Id == item.KlantA select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                    if (klntA != null)
                    {
                        string fulnameKlantA = klntA.Vn.ToString() + " " + klntA.An.ToString();
                        model.KlantA = fulnameKlantA;
                        model.KlantAId = (int)item.KlantA;
                    }
                    else
                    {
                        model.KlantA = "";
                        model.KlantAId = 0;
                    }

                    if (item.BestellingA != null)
                    {
                        model.BestellingAId = (int)item.BestellingA;
                    }
                    else
                    {
                        model.BestellingAId = 0;
                    }


                    var chefA = (from s in _context.Personeel where s.Id == item.KokA select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                    string fulnameA = chefA.Vn.ToString() + " " + chefA.An.ToString();
                    model.KokA = fulnameA;

                    //B---

                    var klntB = (from s in _context.Klanten where s.Id == item.KlantB select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();


                    if (klntB != null)
                    {
                        string fulnameKlantB = klntB.Vn.ToString() + " " + klntB.An.ToString();
                        model.KlantB = fulnameKlantB;
                        model.KlantBId = (int)item.KlantB;
                    }
                    else
                    {
                        model.KlantB = "";
                        model.KlantBId = 0;
                    }

                    if (item.BestellingB != null)
                    {
                        model.BestellingBId = (int)item.BestellingB;
                    }
                    else
                    {
                        model.BestellingBId = 0;
                    }

                    var chefB = (from s in _context.Personeel where s.Id == item.KokB select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                    string fulnameB = chefB.Vn.ToString() + " " + chefB.An.ToString();
                    model.KokB = fulnameB;

                    //add to list

                    tijdslotenOverzicht.Add(model);
                }
            }
                


            return View(tijdslotenOverzicht);
        }

        // GET: TijdSloten/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblTijdSloten = await _context.TijdSloten
                .Include(t => t.KoksOnDuty)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblTijdSloten == null)
            {
                return NotFound();
            }

            return View(tblTijdSloten);
        }

        // GET: TijdSloten/Create
        public IActionResult Create()
        {
            ViewData["KoksOnDutyId"] = new SelectList(_context.KoksOnDuties, "Id", "Id");
            return View();
        }


        public IActionResult TijdSloten()
        {
 
            DateTime vandaag = DateTime.Today;
            DateTime  selectie = (DateTime)TempData["GekozenTijdslotDatum"];
            ViewBag.Datum = selectie.ToShortDateString();
            var temp = from c in _context.TijdSloten where c.Tijdslot.Date == vandaag.Date select c;



            if (temp != null)
            {
                
                if(selectie == DateTime.MinValue)

                {
                    var src = DateTime.Now;
                    var hm = new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);
                    var data = _context.TijdSloten.Where(s => s.Tijdslot > hm).ToList();

                    List<TijdSlotenModel> tijdSlots = new();
                    foreach (var item in data)
                    {
                        TijdSlotenModel tijdSlot = new()
                        {
                            Id = item.Id,
                            KokAId = item.KokA,
                            KokBId = item.KokB,
                            slotAChecked = item.slotAChecked,
                            slotBChecked = item.slotBChecked,
                            TijdSlot = item.Tijdslot
                        };

                        var chefA = (from s in _context.Personeel where s.Id == item.KokA select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                        string fulnameA = chefA.Vn.ToString() + " " + chefA.An.ToString();
                        tijdSlot.KokA = fulnameA;

                        var chefB = (from s in _context.Personeel where s.Id == item.KokB select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                        string fulnameB = chefB.Vn.ToString() + " " + chefB.An.ToString();
                        tijdSlot.KokB = fulnameB;

                        tijdSlots.Add(tijdSlot);
                    }

                    List<TijdSlotenModel> tijdSlotsOrderd = new();
                    tijdSlotsOrderd = tijdSlots.OrderBy(s => s.TijdSlot).ToList();
                    return View(tijdSlotsOrderd);
                }
                
                else
                {
                    var ts = from c in _context.TijdSloten where c.Tijdslot.Date == selectie.Date select c;
                    var src = DateTime.Now;
                    var hm = new DateTime(src.Year, src.Month, src.Day, src.Hour, src.Minute, 0);
                    var data = ts.Where(s => s.Tijdslot > hm).ToList();

                    List<TijdSlotenModel> tijdSlots = new();
                    foreach (var item in data)
                    {
                        TijdSlotenModel tijdSlot = new()
                        {
                            Id = item.Id,
                            KokAId = item.KokA,
                            KokBId = item.KokB,
                            slotAChecked = item.slotAChecked,
                            slotBChecked = item.slotBChecked,
                            TijdSlot = item.Tijdslot
                        };
                        

                        var chef = (from s in _context.Personeel where s.Id == item.KokA select new {Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                        string fulnameA = chef.Vn.ToString()+" "+ chef.An.ToString();
                        tijdSlot.KokA = fulnameA;

                        var chefB = (from s in _context.Personeel where s.Id == item.KokB select new { Vn = s.Voornaam, An = s.Naam }).FirstOrDefault();
                        string fulnameB = chefB.Vn.ToString() + " " + chefB.An.ToString();
                        tijdSlot.KokB = fulnameB;

                        tijdSlots.Add(tijdSlot);
                    }

                    List<TijdSlotenModel> tijdSlotsOrderd = new();
                    tijdSlotsOrderd = tijdSlots.OrderBy(s => s.TijdSlot).ToList();
                    return View(tijdSlotsOrderd);
                }
                
            }
            return RedirectToAction(nameof(Index));
        }

        
        public IActionResult TijdSlot1(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //A> Maak het oude tijdslot vrij 
            //--------------------------------------------------------------
            int bestellingsId = (int)TempData["Bestelling"];
            var slots = _context.TijdSloten.ToList().Where(s => s.BestellingA == bestellingsId || s.BestellingB == bestellingsId);
            int klantIdtemp = 0;
            foreach (var slot in slots)
            {      
                if(slot.BestellingA == bestellingsId)
                {
                    slot.BestellingA = null;
                    slot.slotAChecked = false;
                    klantIdtemp = (int) slot.KlantA;
                    slot.KlantA = null;
                    _context.Update(slot);
                    _context.SaveChanges();
                }
                else 
                {
                    slot.BestellingB = null;
                    slot.slotBChecked = false;
                    klantIdtemp = (int)slot.KlantB;
                    slot.KlantB = null;
                    _context.Update(slot);
                    _context.SaveChanges();
                }
                         
            }
            
            //B> slaag het nieuwe tijdslot op
            //--------------------------------

                //int klantId = (int)TempData["Klant"];
                var tblTijdSloten = _context.TijdSloten.Find(id);
                tblTijdSloten.slotAChecked = true;
                tblTijdSloten.BestellingA = bestellingsId;
                tblTijdSloten.KlantA = klantIdtemp;

                 _context.Update(tblTijdSloten);
                 _context.SaveChanges();

                //error opvang
                if (tblTijdSloten == null)
                {
                    return NotFound();
                }
            
            //C> Passeer data terug naar de bestelling
            //----------------------------------------
            TempData["GekozenTijdSlot"] = tblTijdSloten.Tijdslot;
            return RedirectToAction("Edit", "Bestellingen", new { id = tblTijdSloten.BestellingA }); 
        }
        public async Task<IActionResult> TijdSlot2(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //A> Maak het oude tijdslot vrij 
            //--------------------------------------------------------------
            int bestellingsId = (int)TempData["Bestelling"];
            var slots = _context.TijdSloten.ToList().Where(s => s.BestellingA == bestellingsId || s.BestellingB == bestellingsId);
            int klantIdtemp = 0;
            foreach (var slot in slots)
            {
                if (slot.BestellingA == bestellingsId)
                {
                    slot.BestellingA = null;
                    slot.slotAChecked = false;
                    klantIdtemp = (int)slot.KlantA;
                    slot.KlantA = null;
                    _context.Update(slot);
                    _context.SaveChanges();
                }
                else
                {
                    slot.BestellingB = null;
                    slot.slotBChecked = false;
                    klantIdtemp = (int)slot.KlantB;
                    slot.KlantB = null;
                    _context.Update(slot);
                    _context.SaveChanges();
                }

            }

            //B> slaag het nieuwe tijdslot op
            //--------------------------------          
            //int klantId = (int)TempData["Klant"];
                var tblTijdSloten = await _context.TijdSloten.FindAsync(id);
                tblTijdSloten.slotBChecked = true;
                tblTijdSloten.BestellingB = bestellingsId;
                tblTijdSloten.KlantB = klantIdtemp;

                _context.Update(tblTijdSloten);
                await _context.SaveChangesAsync();

                 //error opvang
                 if (tblTijdSloten == null)
                 {
                    return NotFound();
                 }

            //C> Passeer data terug naar de bestelling
            //----------------------------------------
                TempData["GekozenTijdSlot"] = tblTijdSloten.Tijdslot;
                return RedirectToAction("Edit", "Bestellingen", new { id = tblTijdSloten.BestellingB });
        }

        // POST: TijdSloten/Create
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Tijdslot,KlantA,BestellingA,KokA,KlantB,BestellingB,KokB,KoksOnDutyId")] TblTijdSloten tblTijdSloten)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblTijdSloten);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["KoksOnDutyId"] = new SelectList(_context.KoksOnDuties, "Id", "Id", tblTijdSloten.KoksOnDutyId);
            return View(tblTijdSloten);
        }

        // GET: TijdSloten/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblTijdSloten = await _context.TijdSloten.FindAsync(id);
            if (tblTijdSloten == null)
            {
                return NotFound();
            }
            ViewData["KoksOnDutyId"] = new SelectList(_context.KoksOnDuties, "Id", "Id", tblTijdSloten.KoksOnDutyId);
            return View(tblTijdSloten);
        }

        // POST: TijdSloten/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Tijdslot,KlantA,BestellingA,KokA,KlantB,BestellingB,KokB,KoksOnDutyId")] TblTijdSloten tblTijdSloten)
        {
            if (id != tblTijdSloten.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblTijdSloten);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblTijdSlotenExists(tblTijdSloten.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["KoksOnDutyId"] = new SelectList(_context.KoksOnDuties, "Id", "Id", tblTijdSloten.KoksOnDutyId);
            return View(tblTijdSloten);
        }

        // GET: TijdSloten/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblTijdSloten = await _context.TijdSloten
                .Include(t => t.KoksOnDuty)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblTijdSloten == null)
            {
                return NotFound();
            }

            return View(tblTijdSloten);
        }

        // POST: TijdSloten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblTijdSloten = await _context.TijdSloten.FindAsync(id);
            _context.TijdSloten.Remove(tblTijdSloten);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblTijdSlotenExists(int id)
        {
            return _context.TijdSloten.Any(e => e.Id == id);
        }
    }
}
