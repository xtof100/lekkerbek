﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class PersoneelController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public PersoneelController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Personeel
        public async Task<IActionResult> Index()
        {
            return View(await _context.Personeel.ToListAsync());
        }

        // GET: Personeel/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblPersoneel = await _context.Personeel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblPersoneel == null)
            {
                return NotFound();
            }

            return View(tblPersoneel);
        }

        // GET: Personeel/Create
        public IActionResult Create()
        {
            ViewData["RolId"] = new SelectList((from s in _context.Rol.ToList()
                                                  select new { Id = s.Id, Rol = s.Rol }), "Id", "Rol", null);
            return View();
        }

        // POST: Personeel/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(PersoneelModel model)
        {
            if (ModelState.IsValid)
            {
                //get details
                TblPersoneelDetails details = new();

                details.Straat = model.Straat;
                details.Huisnr = model.Huisnr;
                details.Postcode = model.Postcode;
                details.Stad = model.Stad;
                details.Email = model.Email;
                details.Telefoon = model.Telefoon;
                details.GeboorteDatum = model.GeboorteDatum;

                //save details zodat er een id word aangemaakt
                _context.Add(details);
                _context.SaveChanges();

                //get details id
                int detailsId = _context.PersoneelDetails.ToList().OrderByDescending(s => s.Id).FirstOrDefault().Id;

                //Get basic personeel info
                TblPersoneel personeel = new();

                personeel.PersoneelDetailsId = detailsId;

                personeel.Voornaam = model.Voornaam;
                personeel.Naam = model.Naam;
                personeel.RolId = model.RolId;

                _context.Add(personeel);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Personeel/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            PersoneelModel model = new();
            var tblPersoneel = await _context.Personeel.FindAsync(id);
            var tblPersoneelDetails = await _context.PersoneelDetails.FindAsync(tblPersoneel.PersoneelDetailsId);

            model.PersoneelId = tblPersoneel.Id;
            model.PersoneelDetailsId = tblPersoneelDetails.Id;

            model.RolId = tblPersoneel.RolId;
            model.Naam = tblPersoneel.Naam;
            model.Voornaam = tblPersoneel.Voornaam;
            model.GeboorteDatum = tblPersoneelDetails.GeboorteDatum;
            model.Straat = tblPersoneelDetails.Straat;
            model.Huisnr = tblPersoneelDetails.Huisnr;
            model.Postcode = tblPersoneelDetails.Postcode;
            model.Stad = tblPersoneelDetails.Stad;
            model.Email = tblPersoneelDetails.Email;
            model.Telefoon = tblPersoneelDetails.Telefoon;


            if (tblPersoneel == null)
            {
                return NotFound();
            }
            if (tblPersoneelDetails == null)
            {
                return NotFound();
            }

            ViewData["RolId"] = new SelectList((from s in _context.Rol.ToList()
                                                select new { Id = s.Id, Rol = s.Rol }), "Id", "Rol",model.RolId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(PersoneelModel model)
        {
            if (ModelState.IsValid)
            {
                TblPersoneel personeel = new();
                TblPersoneelDetails details = new();

                details.Id = model.PersoneelDetailsId;
                details.Straat = model.Straat;
                details.Huisnr = model.Huisnr;
                details.Postcode = model.Postcode;
                details.Stad = model.Stad;
                details.Email = model.Email;
                details.Telefoon = model.Telefoon;
                details.GeboorteDatum = model.GeboorteDatum;

                _context.Update(details);
                _context.SaveChanges();

                personeel.Id = model.PersoneelId;

                personeel.Voornaam = model.Voornaam;
                personeel.Naam = model.Naam;
                personeel.RolId = model.RolId;

                personeel.PersoneelDetailsId = model.PersoneelDetailsId;

                _context.Update(personeel);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(model);
            }
        }

        // GET: Personeel/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblPersoneel = await _context.Personeel
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblPersoneel == null)
            {
                return NotFound();
            }

            return View(tblPersoneel);
        }

        // POST: Personeel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblPersoneel = await _context.Personeel.FindAsync(id);
            int PersoneelDetailsId = tblPersoneel.PersoneelDetailsId;

            var tblDetail = await _context.PersoneelDetails.FindAsync(PersoneelDetailsId);

            _context.Personeel.Remove(tblPersoneel);
            _context.PersoneelDetails.Remove(tblDetail);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblPersoneelExists(int id)
        {
            return _context.Personeel.Any(e => e.Id == id);
        }
    }
}
