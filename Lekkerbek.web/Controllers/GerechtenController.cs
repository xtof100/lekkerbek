﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;

namespace Lekkerbek.web.Controllers
{
    public class GerechtenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public GerechtenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Gerechten
        public async Task<IActionResult> Index()
        {
            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;
            return View(await _context.Gerechten.ToListAsync());
        }

        // GET: Gerechten/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Gerechten == null)
            {
                return NotFound();
            }

            var tblGerechten = await _context.Gerechten
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblGerechten == null)
            {
                return NotFound();
            }

            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;
            return View(tblGerechten);
        }

        // GET: Gerechten/Create
        public IActionResult Create()
        {
            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;
            return View();
        }

        // POST: Gerechten/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,GerechtNaam,Beschrijving,Prijs")] TblGerechten tblGerechten)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblGerechten);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblGerechten);
        }

        // GET: Gerechten/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Gerechten == null)
            {
                return NotFound();
            }

            var tblGerechten = await _context.Gerechten.FindAsync(id);
            if (tblGerechten == null)
            {
                return NotFound();
            }

            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;
            return View(tblGerechten);
        }

        // POST: Gerechten/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,GerechtNaam,Beschrijving,Prijs")] TblGerechten tblGerechten)
        {
            if (id != tblGerechten.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblGerechten);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblGerechtenExists(tblGerechten.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblGerechten);
        }

        // GET: Gerechten/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Gerechten == null)
            {
                return NotFound();
            }

            var tblGerechten = await _context.Gerechten
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblGerechten == null)
            {
                return NotFound();
            }

            ViewData["Layout"] = _context.LoginData.FirstOrDefault().Layout;
            return View(tblGerechten);
        }

        // POST: Gerechten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Gerechten == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Gerechten'  is null.");
            }
            var tblGerechten = await _context.Gerechten.FindAsync(id);
            if (tblGerechten != null)
            {
                _context.Gerechten.Remove(tblGerechten);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblGerechtenExists(int id)
        {
          return _context.Gerechten.Any(e => e.Id == id);
        }
    }
}
