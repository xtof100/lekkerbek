﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;

namespace Lekkerbek.web.Controllers
{
    public class GerechtOptiesController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public GerechtOptiesController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: GerechtOpties
        public async Task<IActionResult> Index()
        {
              return View(await _context.GerechtOpties.ToListAsync());
        }

        // GET: GerechtOpties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.GerechtOpties == null)
            {
                return NotFound();
            }

            var tblGerechtOpties = await _context.GerechtOpties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblGerechtOpties == null)
            {
                return NotFound();
            }

            return View(tblGerechtOpties);
        }

        // GET: GerechtOpties/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: GerechtOpties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Optie,Beschrijving,Prijs")] TblGerechtOpties tblGerechtOpties)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblGerechtOpties);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblGerechtOpties);
        }

        // GET: GerechtOpties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.GerechtOpties == null)
            {
                return NotFound();
            }

            var tblGerechtOpties = await _context.GerechtOpties.FindAsync(id);
            if (tblGerechtOpties == null)
            {
                return NotFound();
            }
            return View(tblGerechtOpties);
        }

        // POST: GerechtOpties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Optie,Beschrijving")] TblGerechtOpties tblGerechtOpties)
        {
            if (id != tblGerechtOpties.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblGerechtOpties);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblGerechtOptiesExists(tblGerechtOpties.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblGerechtOpties);
        }

        // GET: GerechtOpties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.GerechtOpties == null)
            {
                return NotFound();
            }

            var tblGerechtOpties = await _context.GerechtOpties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblGerechtOpties == null)
            {
                return NotFound();
            }

            return View(tblGerechtOpties);
        }

        // POST: GerechtOpties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.GerechtOpties == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.TblGerechtOpties'  is null.");
            }
            var tblGerechtOpties = await _context.GerechtOpties.FindAsync(id);
            if (tblGerechtOpties != null)
            {
                _context.GerechtOpties.Remove(tblGerechtOpties);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblGerechtOptiesExists(int id)
        {
          return _context.GerechtOpties.Any(e => e.Id == id);
        }
    }
}
