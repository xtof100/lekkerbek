﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class KlantenController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public KlantenController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Klanten
        public async Task<IActionResult> Index()
        {
            ViewData["LoginData"] = _context.LoginData.FirstOrDefault();
            return View(await _context.Klanten.ToListAsync());
        }

        // GET: Klanten/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblKlant = await _context.Klanten
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblKlant == null)
            {
                return NotFound();
            }

            return View(tblKlant);
        }

        // GET: Klanten/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Klanten/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(KlantModel model)
        {

            if (ModelState.IsValid)
            {
                //get details
                TblKlantDetails details = new();
            
                details.Straat = model.Straat;
                details.Huisnr = model.Huisnr;
                details.Postcode = model.Postcode;
                details.Stad = model.Stad;
                details.Email = model.Email;    
                details.Telefoon = model.Telefoon;

                //save details zodat er een id word aangemaakt
                _context.Add(details);
                _context.SaveChanges();

                //get details id
                int detailsId =  _context.KlantDetails.ToList().OrderByDescending(s => s.Id).FirstOrDefault().Id;

                //Get basic klant info
                TblKlant klant = new();

                klant.KlantDetailsId = detailsId;

                klant.Voornaam = model.Voornaam;
                klant.Naam = model.Naam;

                klant.Adres = model.Straat.ToString();
                klant.Adres += " ";
                klant.Adres += model.Huisnr.ToString();
                klant.Adres += " ";
                klant.Adres += model.Postcode.ToString();
                klant.Adres += " ";
                klant.Adres += model.Stad.ToString();

                klant.GeboorteDatum = model.GeboorteDatum;

                 _context.Add(klant);
                 _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        // GET: Klanten/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            KlantModel model = new();
            var tblKlant = await _context.Klanten.FindAsync(id);
            var tblKlantDetails = await _context.KlantDetails.FindAsync(tblKlant.KlantDetailsId);


            model.KlantId = tblKlant.Id;
            model.KlantDetailsId = tblKlantDetails.Id;

            model.Naam = tblKlant.Naam;
            model.Voornaam = tblKlant.Voornaam;
            model.GeboorteDatum = tblKlant.GeboorteDatum;
            model.Straat = tblKlantDetails.Straat;
            model.Huisnr = tblKlantDetails.Huisnr;
            model.Postcode = tblKlantDetails.Postcode;
            model.Stad = tblKlantDetails.Stad;
            model.Email = tblKlantDetails.Email;
            model.Telefoon = tblKlantDetails.Telefoon;

            if (tblKlant == null)
            {
                return NotFound();
            }
            if (tblKlantDetails == null)
            {
                return NotFound();
            }


            return View(model);
        }

        // POST: Klanten/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(KlantModel model)
        {

            if (ModelState.IsValid)
            {

                TblKlant klant = new();
                TblKlantDetails details = new();

                details.Id = model.KlantDetailsId;
                details.Straat = model.Straat;
                details.Huisnr = model.Huisnr;
                details.Postcode = model.Postcode;
                details.Stad = model.Stad;
                details.Email = model.Email;
                details.Telefoon = model.Telefoon;

                _context.Update(details);
                _context.SaveChanges();

                klant.Id = model.KlantId;
                klant.Naam = model.Naam;
                klant.Voornaam = model.Voornaam;

                klant.Adres = model.Straat.ToString();
                klant.Adres += " ";
                klant.Adres += model.Huisnr.ToString();
                klant.Adres += " ";
                klant.Adres += model.Postcode.ToString();
                klant.Adres += " ";
                klant.Adres += model.Stad.ToString();

                klant.GeboorteDatum = model.GeboorteDatum;
                klant.KlantDetailsId = model.KlantDetailsId;

                _context.Update(klant);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            else 
            {
                return View(model);
            }
        }

        // GET: Klanten/Delete/5
        public async Task<IActionResult> Delete(int? id)            //todo
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblKlant = await _context.Klanten
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblKlant == null)
            {
                return NotFound();
            }

            return View(tblKlant);
        }

        // POST: Klanten/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblKlant = await _context.Klanten.FindAsync(id);
            int klantDetailsId = tblKlant.KlantDetailsId;

            var tblDetail = await _context.KlantDetails.FindAsync(klantDetailsId);

            _context.Klanten.Remove(tblKlant);
            _context.KlantDetails.Remove(tblDetail);

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblKlantExists(int id)
        {
            return _context.Klanten.Any(e => e.Id == id);
        }
    }
}
