﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;
using Lekkerbek.web.Models;

namespace Lekkerbek.web.Controllers
{
    public class FacturatiesController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public FacturatiesController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: Facturaties
        public async Task<IActionResult> Index()
        {
              return View(await _context.Facturaties.ToListAsync());
        }

        // GET: Facturaties/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Facturaties == null)
            {
                return NotFound();
            }

            var tblFacturatie = await _context.Facturaties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblFacturatie == null)
            {
                return NotFound();
            }

            return View(tblFacturatie);
        }

        // GET: Facturaties/Create
        public IActionResult Create(int? id)
        {
            //a> Opvragen van de nodige data uit de database
            //----------------------------------------------
            var bestelling = _context.Bestellingen.Find(id);

            var gerechten = (from s in _context.BesteldeGerechten
                             join c in _context.Gerechten
                              on s.GerechtenId equals c.Id
                             where s.BestellingId == id
                             select new { c.GerechtNaam, c.Prijs }).ToList();

            var gerechtOpties = (from s in _context.BesteldeGerechten
                                 join c in _context.GerechtOpties
                                  on s.GerechtOptiesId equals c.Id
                                 where s.BestellingId == id
                                 select new { c.Optie, c.Prijs }).ToList();

            var klantInfo = (from s in _context.Bestellingen
                             join c in _context.Klanten
                              on s.KlantId equals c.Id
                             where s.Id == id
                             select new { VolledigeNaam = c.Voornaam + " " + c.Naam, c.Adres, c.KlantDetailsId, c.Getrouwheidscore }).FirstOrDefault();

            var klantInfoDetails = _context.KlantDetails.Where(s => s.Id == klantInfo.KlantDetailsId).FirstOrDefault();

            
            //B> Maak een lijst met gerechten
            //---------------------------------
            List<GerechtenModel> gerechtenLijst = new();

            for (int i = 0; i < gerechten.Count; i++)
            {
                GerechtenModel model = new()
                {
                    GerechtNaam = gerechten[i].GerechtNaam,
                    GerechtPrijs = gerechten[i].Prijs,
                    Optie = gerechtOpties[i].Optie,
                    OptiePrijs = gerechtOpties[i].Prijs

                };

                gerechtenLijst.Add(model);
            }
            //C> Berekening van het verschuldigd bedrag
            //-------------------------------------------------------------------------------
            float totaalPrijsZonderBtw = 0;
            float totaalPrijsMetBtw = 0;
            float btwBedrag = 0;
            float kortingBedrag = 0;
            float btw = _context.Instellingen.FirstOrDefault().BtwInDecimaal;
            float korting = _context.Instellingen.FirstOrDefault().KortingWaardeInDecimaal;
            int getrouwheidsLimiet = _context.Instellingen.FirstOrDefault().GetrouwheidScoreLimiet;

            for (int i = 0; i < gerechten.Count; i++)
            {
                totaalPrijsZonderBtw += gerechten[i].Prijs;
                totaalPrijsZonderBtw += gerechtOpties[i].Prijs;
            }

            //C1> Check of de klant recht heeft op een korting
            if (klantInfo.Getrouwheidscore >= getrouwheidsLimiet)
            {
                kortingBedrag = (float)(totaalPrijsZonderBtw * korting); // korting na 3 bestellingen getrouwheidscore in tegeven bij de instellingen
            }

            totaalPrijsMetBtw = (float)((totaalPrijsZonderBtw - korting) * btw);
            btwBedrag = (float)(totaalPrijsMetBtw - totaalPrijsZonderBtw);

            //D> Vullen van het Afrekeningsmodel
            AfrekeningModel afrekening = new()
            {
                //bestelling
                Id = (int)id,
                GerechtenLijst = gerechtenLijst,
                AantalMaaltijden = gerechten.Count,
                AfhaalTijdstip = bestelling.AfhaalTijdstip,

                //klant
                KlantNaam = klantInfo.VolledigeNaam,
                Adres = klantInfo.Adres,
                Straat = klantInfoDetails.Straat,
                HuisNr = klantInfoDetails.Huisnr,
                Postcode = klantInfoDetails.Postcode,
                Stad = klantInfoDetails.Stad,
                Email = klantInfoDetails.Email,
                Telefoon = klantInfoDetails.Telefoon,
                Getrouwheidscore = klantInfo.Getrouwheidscore,

                //layout
                Layout = _context.LoginData.FirstOrDefault().Layout,

                //facturatie
                PrijsTotaalZonderBtw = (float)Math.Round(totaalPrijsZonderBtw,2),
                KortingBedrag = (float)Math.Round((totaalPrijsZonderBtw * 0.1),2),
                BtwBedrag = (float)Math.Round(btwBedrag,2),
                PrijsTotaalMetBtw =(float)Math.Round(((totaalPrijsZonderBtw - korting)*btw),2),

             };

            //E> Selectie box betaalwijzen
            //---------------------------
            //klanten selectbox
            ViewData["BetaalWijze"] = new SelectList((from s in _context.BetaalWijzen.ToList()
                                                  select new { s.Id, s.BetaalWijze }), "Id", "BetaalWijze", null);


            return View(afrekening);
        }

        // POST: Facturaties/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Create(AfrekeningModel model, int? id)
        {
            if (ModelState.IsValid)
            {

                //A> Data voor de facturatie tabel
                TblFacturatie facturatie = new()
                {
                    PrijsTotaalMetBtw = model.PrijsTotaalMetBtw,
                    BtwBedrag = model.BtwBedrag,
                    Korting = model.KortingBedrag,
                    PrijsTotaalZonderBtw = model.PrijsTotaalZonderBtw,
                    BestellingId = (int)id
                };

                _context.Facturaties.Add(facturatie);
                _context.SaveChanges();

                //B> Data voor de afgehandelde tabel
                TblAfgehandeld afgehandeld = new()
                {
                    FacturatieId = _context.Facturaties.OrderByDescending(s => s.Id).FirstOrDefault().Id,
                    Afgehandeld = true,
                    TijdstipBetaling = DateTime.Now,
                    BetaalWijzeId = model.BetaalWijzeId
                };

                _context.Afgehandeld.Add(afgehandeld);
                _context.SaveChanges();

                //C> Update de getrouwheidscore van de klant
                var klant = (from s in _context.Bestellingen
                                 join c in _context.Klanten
                                  on s.KlantId equals c.Id
                                 where s.Id == id
                                 select c).FirstOrDefault();
                var instellingen = _context.Instellingen.FirstOrDefault();

                   if(klant.Getrouwheidscore == instellingen.GetrouwheidScoreLimiet)
                    {
                        klant.Getrouwheidscore = 0;
                    }
                    else 
                    {
                        klant.Getrouwheidscore ++;
                    }

                    _context.Update(klant);
                    _context.SaveChanges();
                    return RedirectToAction("index","Bestellingen");
            }
            return RedirectToAction("msg");
        }

       
       
        public IActionResult msg()
        {
            //>Layout en rol data
            ViewBag.Layout = _context.LoginData.FirstOrDefault().Layout;
            ViewBag.Rol = _context.LoginData.FirstOrDefault().Rol;

            return View();
        }


        // GET: Facturaties/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Facturaties == null)
            {
                return NotFound();
            }

            var tblFacturatie = await _context.Facturaties.FindAsync(id);
            if (tblFacturatie == null)
            {
                return NotFound();
            }
            return View(tblFacturatie);
        }

        // POST: Facturaties/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PrijsTotaalZonderKorting,Korting,PrijsTotaalMetKorting")] TblFacturatie tblFacturatie)
        {
            if (id != tblFacturatie.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblFacturatie);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblFacturatieExists(tblFacturatie.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblFacturatie);
        }

        // GET: Facturaties/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Facturaties == null)
            {
                return NotFound();
            }

            var tblFacturatie = await _context.Facturaties
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblFacturatie == null)
            {
                return NotFound();
            }

            return View(tblFacturatie);
        }

        // POST: Facturaties/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Facturaties == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Facturaties'  is null.");
            }
            var tblFacturatie = await _context.Facturaties.FindAsync(id);
            if (tblFacturatie != null)
            {
                _context.Facturaties.Remove(tblFacturatie);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblFacturatieExists(int id)
        {
          return _context.Facturaties.Any(e => e.Id == id);
        }
    }
}
