﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;

namespace Lekkerbek.web.Controllers
{
    public class KlantDetailsController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public KlantDetailsController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: KlantDetails
        public async Task<IActionResult> Index()
        {
              return View(await _context.KlantDetails.ToListAsync());
        }

        // GET: KlantDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.KlantDetails == null)
            {
                return NotFound();
            }

            var tblKlantDetails = await _context.KlantDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblKlantDetails == null)
            {
                return NotFound();
            }

            ViewBag.Klant = _context.Klanten.ToList().Where(s => s.KlantDetailsId == tblKlantDetails.Id).FirstOrDefault();


            return View(tblKlantDetails);
        }

        // GET: KlantDetails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: KlantDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Straat,Huisnr,Postcode,Stad,Email,Telefoon")] TblKlantDetails tblKlantDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblKlantDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblKlantDetails);
        }

        // GET: KlantDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.KlantDetails == null)
            {
                return NotFound();
            }

            var tblKlantDetails = await _context.KlantDetails.FindAsync(id);
            if (tblKlantDetails == null)
            {
                return NotFound();
            }
            return View(tblKlantDetails);
        }

        // POST: KlantDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Straat,Huisnr,Postcode,Stad,Email,Telefoon")] TblKlantDetails tblKlantDetails)
        {
            if (id != tblKlantDetails.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblKlantDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblKlantDetailsExists(tblKlantDetails.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblKlantDetails);
        }

        // GET: KlantDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.KlantDetails == null)
            {
                return NotFound();
            }

            var tblKlantDetails = await _context.KlantDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblKlantDetails == null)
            {
                return NotFound();
            }

            return View(tblKlantDetails);
        }

        // POST: KlantDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.KlantDetails == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.TblKlantDetails'  is null.");
            }
            var tblKlantDetails = await _context.KlantDetails.FindAsync(id);
            if (tblKlantDetails != null)
            {
                _context.KlantDetails.Remove(tblKlantDetails);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblKlantDetailsExists(int id)
        {
          return _context.KlantDetails.Any(e => e.Id == id);
        }
    }
}
