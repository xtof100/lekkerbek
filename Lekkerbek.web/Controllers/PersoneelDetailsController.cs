﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.web.Entities;

namespace Lekkerbek.web.Controllers
{
    public class PersoneelDetailsController : Controller
    {
        private readonly LekkerbekDbContext _context;

        public PersoneelDetailsController(LekkerbekDbContext context)
        {
            _context = context;
        }

        // GET: PersoneelDetails
        public async Task<IActionResult> Index()
        {
              return View(await _context.PersoneelDetails.ToListAsync());
        }

        // GET: PersoneelDetails/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.PersoneelDetails == null)
            {
                return NotFound();
            }

            var tblPersoneelDetails = await _context.PersoneelDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblPersoneelDetails == null)
            {
                return NotFound();
            }

            ViewBag.Klant = _context.Personeel.ToList().Where(s => s.PersoneelDetailsId == tblPersoneelDetails.Id).FirstOrDefault();


            return View(tblPersoneelDetails);
        }

        // GET: PersoneelDetails/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PersoneelDetails/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Straat,Huisnr,Postcode,Stad,Email,Telefoon,GeboorteDatum")] TblPersoneelDetails tblPersoneelDetails)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblPersoneelDetails);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblPersoneelDetails);
        }

        // GET: PersoneelDetails/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.PersoneelDetails == null)
            {
                return NotFound();
            }

            var tblPersoneelDetails = await _context.PersoneelDetails.FindAsync(id);
            if (tblPersoneelDetails == null)
            {
                return NotFound();
            }
            return View(tblPersoneelDetails);
        }

        // POST: PersoneelDetails/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Straat,Huisnr,Postcode,Stad,Email,Telefoon,GeboorteDatum")] TblPersoneelDetails tblPersoneelDetails)
        {
            if (id != tblPersoneelDetails.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblPersoneelDetails);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblPersoneelDetailsExists(tblPersoneelDetails.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblPersoneelDetails);
        }

        // GET: PersoneelDetails/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.PersoneelDetails == null)
            {
                return NotFound();
            }

            var tblPersoneelDetails = await _context.PersoneelDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblPersoneelDetails == null)
            {
                return NotFound();
            }

            return View(tblPersoneelDetails);
        }

        // POST: PersoneelDetails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.PersoneelDetails == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.PersoneelDetails'  is null.");
            }
            var tblPersoneelDetails = await _context.PersoneelDetails.FindAsync(id);
            if (tblPersoneelDetails != null)
            {
                _context.PersoneelDetails.Remove(tblPersoneelDetails);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblPersoneelDetailsExists(int id)
        {
          return _context.PersoneelDetails.Any(e => e.Id == id);
        }
    }
}
