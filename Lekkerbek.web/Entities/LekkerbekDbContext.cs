﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lekkerbek.web.Models;
using Lekkerbek.web.Entities;

namespace Lekkerbek.web.Entities
{
    public class LekkerbekDbContext : DbContext
    {

        public LekkerbekDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<TblKlant> Klanten { get; set; }
        public DbSet<TblKlantDetails> KlantDetails { get; set; }
        public DbSet<TblBestelling>Bestellingen { get; set; }
        public DbSet<TblGerechten>Gerechten { get; set; }
        public DbSet<TblGerechtOpties> GerechtOpties { get; set; }
        public DbSet<TblPersoneel>Personeel { get; set; }
        public DbSet<TblPersoneelDetails> PersoneelDetails { get; set; }
        public DbSet<TblRol> Rol { get; set; }
        public DbSet<TblBesteldeGerechten> BesteldeGerechten { get; set; }
        public DbSet<TblKoksOnDuty> KoksOnDuties { get; set; }
        public DbSet<TblTijdSloten> TijdSloten { get; set; }
        public DbSet<TblFacturatie> Facturaties { get; set; }
        public DbSet<TblAfgehandeld> Afgehandeld { get; set; }
        public DbSet<TblBetaalWijzen> BetaalWijzen { get; set; }
        public DbSet<TblLoginData> LoginData { get; set; }
        public DbSet<TblInstellingen> Instellingen { get; set; }

    }
}
