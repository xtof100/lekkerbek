﻿using System.Collections.Generic;

namespace Lekkerbek.web.Entities
{
    public class TblBetaalWijzen
    {
        public int Id { get; set; }
        public string BetaalWijze { get; set; }

      
        public virtual ICollection<TblAfgehandeld> Afgehandeld { get; set; }
    }
}
