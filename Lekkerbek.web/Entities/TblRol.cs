﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblRol
    {
        public int Id { get; set; }
        public string Rol { get; set; }
    }
}
