﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblGerechten
    {
        public int Id { get; set; } //PK
        public string GerechtNaam { get; set; }
        public string Beschrijving { get; set; }
        public float Prijs { get; set; }

        public virtual ICollection<TblBesteldeGerechten> BesteldeGerechten { get; set; } //FK 
        
    }
}
