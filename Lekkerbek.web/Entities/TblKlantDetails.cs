﻿namespace Lekkerbek.web.Entities
{
    public class TblKlantDetails
    {
        public int Id { get; set; }
        public string Straat { get; set; }
        public string Huisnr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }


        public virtual TblKlant Klant { get; set; }
    }
}
