﻿using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.web.Entities
{
    public class TblInstellingen
    {
        //Adres van het bedrijf voor op de factuur en bestelbon
        public int Id { get; set; }
        public string Naam { get; set; }
        public string Straat { get; set; }
        public string Huisnr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }

        //Instellingen voor de tijdsloten
        public int TijdslotInterval { get; set; }

        //instellingen voor de klantenkorting
        public int GetrouwheidScoreLimiet { get; set; }     
        public float KortingWaardeInDecimaal { get; set; }

        //algemene instellingen
        public float BtwInDecimaal { get; set; }

        //Email instellingen
        public string AfzenderAdres { get; set; }
        public string AfzenderNaam { get; set; }
        public string Onderwerp { get; set; }
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string Login { get; set; }
        public string Paswoord { get; set; }

    }
}
