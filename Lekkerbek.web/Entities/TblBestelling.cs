﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblBestelling
    {
        public int Id { get; set; } //PK
        public int? KlantId { get; set; } //FK not null
        public DateTime AfhaalTijdstip { get; set; } //not null
        public int? AantalMaaltijden { get; set; } // not null
        
       

        public virtual TblKlant Klant { get; set; } //FK klant
        public virtual TblFacturatie Facturatie { get; set; } //FK klant


        public virtual ICollection<TblBesteldeGerechten> BesteldeGerechten { get; set; } //FK bestelde gerechten

    }
}
