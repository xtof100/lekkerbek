﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblBesteldeGerechten
    { 
        public int Id { get; set; } //PK
        public int GerechtenId { get; set; } //FK
        public int GerechtOptiesId { get; set; } //FK
        public int BestellingId { get; set; } //FK

        public virtual TblGerechten Gerechten { get; set; } //FK Gerechten 
        public virtual TblGerechtOpties GerechtOpties { get; set; } //FK Gerechten 
        public virtual TblBestelling Bestelling { get; set; }

    }
}
