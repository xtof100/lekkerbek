﻿namespace Lekkerbek.web.Entities
{
    public class TblFacturatie
    {
        public int Id { get; set; }
        public float PrijsTotaalZonderBtw{ get; set; }
        public float Korting { get; set; }
        public float BtwBedrag { get; set; }
        public float PrijsTotaalMetBtw { get; set; }
        public int BestellingId { get; set; }


        public virtual TblBestelling Bestelling { get; set; }
        public virtual TblAfgehandeld Afgehandeld { get; set; }
    }
}
