﻿using System.Collections.Generic;

namespace Lekkerbek.web.Entities
{
    public class TblGerechtOpties
    {
        public int Id { get; set; }
        public string Optie { get; set; }
        public string Beschrijving { get; set; }
        public float Prijs { get; set; }

        public virtual ICollection<TblBesteldeGerechten> BesteldeGerechten { get; set; } //FK 
    }
}
