﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblPersoneel
    {
        public int Id { get; set; }
        public int? RolId { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public int PersoneelDetailsId { get; set; }

        public virtual TblRol Rol { get; set; }
        public virtual TblPersoneelDetails PersoneelDetails { get; set; }

    }
}
