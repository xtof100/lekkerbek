﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblKlant
    {
        public int Id { get; set; } //PK
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public string Adres { get; set; }
        public DateTime GeboorteDatum { get; set; }
        public int Getrouwheidscore { get; set; }
        public string VoorkeursGerechten { get; set; }
        public int KlantDetailsId { get; set; }



        public virtual TblKlantDetails KlantDetails { get; set; } 
        public virtual ICollection <TblBestelling> Bestellingen { get; set; }
    }
}
