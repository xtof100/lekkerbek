﻿using System;

namespace Lekkerbek.web.Entities
{
    public class TblPersoneelDetails
    {
        public int Id { get; set; }
        public string Straat { get; set; }
        public string Huisnr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }
        public DateTime GeboorteDatum { get; set; }

        public virtual TblPersoneel Personeel { get; set; }

    }
}
