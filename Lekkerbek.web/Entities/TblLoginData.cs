﻿namespace Lekkerbek.web.Entities
{
    public class TblLoginData
    {
        public int Id { get; set; }
        public string Rol { get; set; }
        public string Layout { get; set; }
        
    }
}
