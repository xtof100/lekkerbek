﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblKoksOnDuty
    {
        public int Id { get; set; }
        public int KokA { get; set; } //personeelsId
        public int KokB { get; set; } //personeelsId

        [DataType(DataType.Date)] //only datum no time
        public DateTime Datum { get; set; }
        
        [DataType(DataType.Time)] //only time no date
        public DateTime ShiftStart { get; set; }
        
        [DataType(DataType.Time)]
        public DateTime ShiftStop{ get; set; }


        public virtual ICollection<TblTijdSloten> TijdSloten { get; set; }
    } 
}

