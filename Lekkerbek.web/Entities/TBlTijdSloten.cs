﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Entities
{
    public class TblTijdSloten
    {
        public int Id { get; set; }
        public DateTime Tijdslot { get; set; }
        public int? KlantA { get; set; } //klantId
        public int? BestellingA { get; set; } //bestellingId
        public int KokA { get; set; } //KoksOnDutyId
        public bool slotAChecked { get; set; }
        public int? KlantB { get; set; } //klantId
        public int? BestellingB { get; set; } //bestellingId
        public int KokB { get; set; } //KoksOnDutyId
        public bool slotBChecked { get; set; }
        public int KoksOnDutyId { get; set; }

        public virtual TblKoksOnDuty KoksOnDuty { get; set; } //FK

    }
}
