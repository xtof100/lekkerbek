﻿using System;

namespace Lekkerbek.web.Entities
{
    public class TblAfgehandeld
    {
        public int Id { get; set; }
        public int FacturatieId { get; set; }
        public bool Afgehandeld { get; set; }
        public DateTime TijdstipBetaling { get; set; }
        public int BetaalWijzeId { get; set; }

        public virtual TblFacturatie Facturatie { get; set; }
        public virtual TblBetaalWijzen BetaalWijze { get; set; }
    }
}