﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Models
{
    public class BestellingModel
    {

        public int Id { get; set; }
        public int? KlantId { get; set; }
        public DateTime AfhaalTijdstip { get; set; } 
        public int AantalMaaltijden { get; set; }
       
        //layout
        public string Layout { get; set; }

    }
}
