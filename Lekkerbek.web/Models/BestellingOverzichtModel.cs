﻿using System;

namespace Lekkerbek.web.Models
{
    public class BestellingOverzichtModel
    {
        public int Id { get; set; }
        public DateTime AfhaalTijdstip { get; set; }
        public int? AantalMaaltijden { get; set; }
        public string KlantNaam { get; set; }
        public float TotalePrijs { get; set; }
        public bool Betaald { get; set; }
    }
}
