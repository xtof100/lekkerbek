﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Models
{
    public class GerechtenModel
    {
        public string GerechtNaam { get; set; }
        public string Optie { get; set; }
        public float GerechtPrijs { get; set; }
        public float OptiePrijs { get; set; }
    }
}
