﻿using System;

namespace Lekkerbek.web.Models
{
    public class PersoneelModel
    {
        public int PersoneelId { get; set; }
        public int? RolId { get; set; }
        public string Naam { get; set; }
        public string Voornaam { get; set; }

        public int PersoneelDetailsId { get; set; }
        public string Straat { get; set; }
        public string Huisnr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }
        public DateTime GeboorteDatum { get; set; }
    }
}
