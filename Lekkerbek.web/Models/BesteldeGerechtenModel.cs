﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Models
{
    public class BesteldeGerechtenModel
    {
        public int Id { get; set; }
        public int BestellingId { get; set; } //fK
        public List<string> GerechtNaam { get; set; }
        public List<int> GerechtenId { get; set; } //FK
        public string Gerecht { get; set; }
        public string Optie { get; set; }


        //model aproach dropdown
        public string SelectedGerechtId { get; set; }
        public IEnumerable<SelectListItem> Gerechten { get; set; }

        public string SelectedGerechtOptieId { get; set; }
        public IEnumerable<SelectListItem> GerechtOpties { get; set; }

        //layout
        public string Layout { get; set; }


    }
}
