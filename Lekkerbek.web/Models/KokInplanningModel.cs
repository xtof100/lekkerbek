﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.web.Models
{
    public class KokInplanningModel
    {
        public int Id { get; set; }
        public int KokA { get; set; } //personeelsId
        public int KokB { get; set; } //personeelsId

        public string KokANaam { get; set; } 
        public string KokBNaam { get; set; } 

        [DataType(DataType.Date)] //only datum no time
        public DateTime Datum { get; set; }

        [DataType(DataType.Time)] //only time no date
        public DateTime ShiftStart { get; set; }

        [DataType(DataType.Time)]
        public DateTime ShiftStop { get; set; }

    }
}
