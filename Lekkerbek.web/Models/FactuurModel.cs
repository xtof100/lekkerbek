﻿using System;
using System.Collections.Generic;

namespace Lekkerbek.web.Models
{
    public class FactuurModel
    {
        //bestelling gegevens
        public int Id { get; set; }
        public List<GerechtenModel> GerechtenLijst { get; set; }
        public List<string> GerechtNaam { get; set; }
        public List<string> GerechtOptie { get; set; }
        public List<float> GerechtPrijs { get; set; }
        public List<float> OptiePrijs { get; set; }
        public DateTime AfhaalTijdstip { get; set; }
        public int AantalMaaltijden { get; set; }

        //klant gegevens
        public int KlantId { get; set; }
        public string KlantNaam { get; set; }
        public string  Adres { get; set; }
        public string Straat { get; set; }
        public string HuisNr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }
        
        public String Betaalwijze { get; set; } //al toegevoegd voor later

        //bedrijfsgegevens
        public string BedrijfNaam { get; set; }
        public string BedrijfStraat { get; set; }
        public string BedrijfHuisNr { get; set; }
        public string BedrijfPostcode { get; set; }
        public string BedrijfStad { get; set; }
        public string BedrijfEmail { get; set; }
        public string BedrijfTelefoon { get; set; }

        //layout
        public string Layout { get; set; }
    }
}
