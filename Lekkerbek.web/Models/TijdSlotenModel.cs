﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Lekkerbek.web.Models
{
    public class TijdSlotenModel
    {
        public DateTime TijdSlot { get; set; }
        public string KokA { get; set; }
        public int KokAId { get; set; }
        public string KokB { get; set; }
        public int KokBId { get; set; }
        public bool slotAChecked { get; set; }
        public bool slotBChecked { get; set; }
        public int Selector { get; set; }
        public int Id { get; set; }
        public int BestellingAId { get; set; }
        public int BestellingBId { get; set; }
        public int KlantAId { get; set; }
        public string KlantA { get; set; }
        public int KlantBId { get; set; }
        public string KlantB { get; set; }
        public int KoksOnDutiesId { get; set; }

    }
}
