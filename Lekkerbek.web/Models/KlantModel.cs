﻿using System;

namespace Lekkerbek.web.Models
{
    public class KlantModel
    {
        //klant
        public int KlantId { get; set; } //PK
        public string Naam { get; set; }
        public string Voornaam { get; set; }
        public string Adres { get; set; }
        public int Getrouwheidscore { get; set; }
        public string VoorkeursGerechten { get; set; }
        public DateTime GeboorteDatum { get; set; }


        //klantDetails
        public int KlantDetailsId { get; set; } //zit ook in tblKlant

        public string Straat { get; set; }
        public string Huisnr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }



    }
}
