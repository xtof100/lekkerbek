﻿using System;
using System.Collections.Generic;

namespace Lekkerbek.web.Models
{
    public class AfrekeningModel
    {
        //bestelling gegevens
        public int Id { get; set; }
        public List<GerechtenModel> GerechtenLijst { get; set; }
        public List<string> GerechtNaam { get; set; }
        public List<string> GerechtOptie { get; set; }
        public List<float> GerechtPrijs { get; set; }
        public List<float> OptiePrijs { get; set; }
        public DateTime AfhaalTijdstip { get; set; }
        public int AantalMaaltijden { get; set; }

        //klant gegevens
        public int KlantId { get; set; }
        public string KlantNaam { get; set; }
        public string Adres { get; set; }
        public string Straat { get; set; }
        public string HuisNr { get; set; }
        public string Postcode { get; set; }
        public string Stad { get; set; }
        public string Email { get; set; }
        public string Telefoon { get; set; }
        public int Getrouwheidscore { get; set; }

        //Facturaties tabel
        public int FacturatieId { get; set; }
        public float KortingBedrag { get; set; }
        
        public float PrijsTotaalZonderBtw { get; set; }
        public float PrijsTotaalMetBtw { get; set; }
        public float BtwBedrag { get; set; }

        //Afgehandeld tabel
        public int AfgehandeldId { get; set; }
        public bool Afghandeld { get; set; }
        public DateTime TijdstipBetaling { get; set; }

        //betaalWijze tabel
        public int BetaalWijzeId { get; set; }
        public string BetaalWijze { get; set; }

        //layout
        public string Layout { get; set; }

    }
}