﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.web.Migrations
{
    public partial class dbinit2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Afgehandeld_BetaalWijzeId",
                table: "Afgehandeld");

            migrationBuilder.CreateIndex(
                name: "IX_Afgehandeld_BetaalWijzeId",
                table: "Afgehandeld",
                column: "BetaalWijzeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Afgehandeld_BetaalWijzeId",
                table: "Afgehandeld");

            migrationBuilder.CreateIndex(
                name: "IX_Afgehandeld_BetaalWijzeId",
                table: "Afgehandeld",
                column: "BetaalWijzeId",
                unique: true);
        }
    }
}
