﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.web.Migrations
{
    public partial class dbinit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BetaalWijzen",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BetaalWijze = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BetaalWijzen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gerechten",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GerechtNaam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Beschrijving = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prijs = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gerechten", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GerechtOpties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Optie = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Beschrijving = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Prijs = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GerechtOpties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Instellingen",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Straat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Huisnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Postcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stad = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefoon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TijdslotInterval = table.Column<int>(type: "int", nullable: false),
                    GetrouwheidScoreLimiet = table.Column<int>(type: "int", nullable: false),
                    KortingWaardeInDecimaal = table.Column<float>(type: "real", nullable: false),
                    BtwInDecimaal = table.Column<float>(type: "real", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Instellingen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KlantDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Straat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Huisnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Postcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stad = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefoon = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KlantDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "KoksOnDuties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KokA = table.Column<int>(type: "int", nullable: false),
                    KokB = table.Column<int>(type: "int", nullable: false),
                    Datum = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ShiftStart = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ShiftStop = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KoksOnDuties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoginData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rol = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Layout = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginData", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PersoneelDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Straat = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Huisnr = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Postcode = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stad = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telefoon = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GeboorteDatum = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersoneelDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rol",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Rol = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rol", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Klanten",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Voornaam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Adres = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GeboorteDatum = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Getrouwheidscore = table.Column<int>(type: "int", nullable: false),
                    VoorkeursGerechten = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KlantDetailsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Klanten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Klanten_KlantDetails_KlantDetailsId",
                        column: x => x.KlantDetailsId,
                        principalTable: "KlantDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TijdSloten",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Tijdslot = table.Column<DateTime>(type: "datetime2", nullable: false),
                    KlantA = table.Column<int>(type: "int", nullable: true),
                    BestellingA = table.Column<int>(type: "int", nullable: true),
                    KokA = table.Column<int>(type: "int", nullable: false),
                    slotAChecked = table.Column<bool>(type: "bit", nullable: false),
                    KlantB = table.Column<int>(type: "int", nullable: true),
                    BestellingB = table.Column<int>(type: "int", nullable: true),
                    KokB = table.Column<int>(type: "int", nullable: false),
                    slotBChecked = table.Column<bool>(type: "bit", nullable: false),
                    KoksOnDutyId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TijdSloten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TijdSloten_KoksOnDuties_KoksOnDutyId",
                        column: x => x.KoksOnDutyId,
                        principalTable: "KoksOnDuties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Personeel",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RolId = table.Column<int>(type: "int", nullable: true),
                    Naam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Voornaam = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PersoneelDetailsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Personeel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Personeel_PersoneelDetails_PersoneelDetailsId",
                        column: x => x.PersoneelDetailsId,
                        principalTable: "PersoneelDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Personeel_Rol_RolId",
                        column: x => x.RolId,
                        principalTable: "Rol",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Bestellingen",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    KlantId = table.Column<int>(type: "int", nullable: true),
                    AfhaalTijdstip = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AantalMaaltijden = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bestellingen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bestellingen_Klanten_KlantId",
                        column: x => x.KlantId,
                        principalTable: "Klanten",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "BesteldeGerechten",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GerechtenId = table.Column<int>(type: "int", nullable: false),
                    GerechtOptiesId = table.Column<int>(type: "int", nullable: false),
                    BestellingId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BesteldeGerechten", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BesteldeGerechten_Bestellingen_BestellingId",
                        column: x => x.BestellingId,
                        principalTable: "Bestellingen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BesteldeGerechten_Gerechten_GerechtenId",
                        column: x => x.GerechtenId,
                        principalTable: "Gerechten",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BesteldeGerechten_GerechtOpties_GerechtOptiesId",
                        column: x => x.GerechtOptiesId,
                        principalTable: "GerechtOpties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Facturaties",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PrijsTotaalZonderBtw = table.Column<float>(type: "real", nullable: false),
                    Korting = table.Column<float>(type: "real", nullable: false),
                    BtwBedrag = table.Column<float>(type: "real", nullable: false),
                    PrijsTotaalMetBtw = table.Column<float>(type: "real", nullable: false),
                    BestellingId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facturaties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Facturaties_Bestellingen_BestellingId",
                        column: x => x.BestellingId,
                        principalTable: "Bestellingen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Afgehandeld",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FacturatieId = table.Column<int>(type: "int", nullable: false),
                    Afgehandeld = table.Column<bool>(type: "bit", nullable: false),
                    TijdstipBetaling = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BetaalWijzeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Afgehandeld", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Afgehandeld_BetaalWijzen_BetaalWijzeId",
                        column: x => x.BetaalWijzeId,
                        principalTable: "BetaalWijzen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Afgehandeld_Facturaties_FacturatieId",
                        column: x => x.FacturatieId,
                        principalTable: "Facturaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Afgehandeld_BetaalWijzeId",
                table: "Afgehandeld",
                column: "BetaalWijzeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Afgehandeld_FacturatieId",
                table: "Afgehandeld",
                column: "FacturatieId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BesteldeGerechten_BestellingId",
                table: "BesteldeGerechten",
                column: "BestellingId");

            migrationBuilder.CreateIndex(
                name: "IX_BesteldeGerechten_GerechtenId",
                table: "BesteldeGerechten",
                column: "GerechtenId");

            migrationBuilder.CreateIndex(
                name: "IX_BesteldeGerechten_GerechtOptiesId",
                table: "BesteldeGerechten",
                column: "GerechtOptiesId");

            migrationBuilder.CreateIndex(
                name: "IX_Bestellingen_KlantId",
                table: "Bestellingen",
                column: "KlantId");

            migrationBuilder.CreateIndex(
                name: "IX_Facturaties_BestellingId",
                table: "Facturaties",
                column: "BestellingId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Klanten_KlantDetailsId",
                table: "Klanten",
                column: "KlantDetailsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Personeel_PersoneelDetailsId",
                table: "Personeel",
                column: "PersoneelDetailsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Personeel_RolId",
                table: "Personeel",
                column: "RolId");

            migrationBuilder.CreateIndex(
                name: "IX_TijdSloten_KoksOnDutyId",
                table: "TijdSloten",
                column: "KoksOnDutyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Afgehandeld");

            migrationBuilder.DropTable(
                name: "BesteldeGerechten");

            migrationBuilder.DropTable(
                name: "Instellingen");

            migrationBuilder.DropTable(
                name: "LoginData");

            migrationBuilder.DropTable(
                name: "Personeel");

            migrationBuilder.DropTable(
                name: "TijdSloten");

            migrationBuilder.DropTable(
                name: "BetaalWijzen");

            migrationBuilder.DropTable(
                name: "Facturaties");

            migrationBuilder.DropTable(
                name: "Gerechten");

            migrationBuilder.DropTable(
                name: "GerechtOpties");

            migrationBuilder.DropTable(
                name: "PersoneelDetails");

            migrationBuilder.DropTable(
                name: "Rol");

            migrationBuilder.DropTable(
                name: "KoksOnDuties");

            migrationBuilder.DropTable(
                name: "Bestellingen");

            migrationBuilder.DropTable(
                name: "Klanten");

            migrationBuilder.DropTable(
                name: "KlantDetails");
        }
    }
}
