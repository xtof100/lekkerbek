﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.web.Migrations
{
    public partial class dbinit4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AfzenderAdres",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AfzenderNaam",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Onderwerp",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Paswoord",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmtpHost",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SmtpPort",
                table: "Instellingen",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AfzenderAdres",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "AfzenderNaam",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "Onderwerp",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "Paswoord",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "SmtpHost",
                table: "Instellingen");

            migrationBuilder.DropColumn(
                name: "SmtpPort",
                table: "Instellingen");
        }
    }
}
